#! /usr/bin/env python
""" [Insert one-line description of file.]

Use the following tables:
    1, Weirdness
    2, w_Drips (armor_substance table)
    3, w_Color (armor_color table)
    4, w_Surrounded by (armor_substance table)
    5, w_Sounds (armor_sounds table)
    6, w_Visual display
    7, W_Marks (need table)
    8, W_Odors (armor_scent table)
    9, Blade material
    10, Blade shape
    11, Hilt/Haft materials
    12, Pommel/Grip materials
    13, Crossguard shape
    14, Hilt/Haft shape
    15, Pommel/Grip accents
"""

# import statments
import random
import textwrap
import config
import ArmorFunctions as AF

__author__ = 'Roger E. Burgess, III'
__copyright__ = "Copyright 2014, Red Flag & Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or later"
__version__ = "0.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"


def weapon_generation(d_line=None, f_split=None, s_weapon_type=None):
    """
    """
    if d_line is None:
        d_line = {'FLOAT: Value Low':20, 'FLOAT: Value High':100}

    if f_split is None:
        f_split = 101.0

    # if s_weapon_type = None:
    # Located in the body of the else: statement below.

    wg_s_goods = ''
    f_goods_value_low = float(d_line['FLOAT: Value Low'])
    f_goods_value_high = float(d_line['FLOAT: Value High'])
    s_price = random.uniform(f_goods_value_low, f_goods_value_high)

    if f_goods_value_low > f_split:
        s_goods_temp = (u'[VALUE]: Swinging this monstrosity '
                        u'around would cause a Green Man to '
                        u'giggle. You might get {:,.2f} sp '
                        u'from a smith with a sense of humor.'
                        u'\n').format(f_split)

        wg_s_goods += textwrap.fill(s_goods_temp, initial_indent=
        '        ', subsequent_indent=
                                 '        ')
    else:
        (l_ammo,
        l_blades,
        l_hafted,
        l_polearms,
        l_misc,
        l_missile,
        l_guns,
        l_tech_blades,
        l_lasers,
        l_tech_missiles,
        l_tech_hvy,
        l_daggers) = weapon_type_list_creator()
        
        l_weap_select = config.csv_to_list('data/weapons/weapon_type_roll.csv')
        i_weap_select = random.randint(1, 400)
        if s_weapon_type is None:
            for line in l_weap_select:
                if i_weap_select <= int(line['DieRoll']):
                    s_weapon_type = line['Type']
                    break

        # hardcoded: 10% of all special weapons have 'purposes'
        i_purpose_roll = random.randint(1, 100)
        # CHANGE BACK to 90 when done.
        if i_purpose_roll >= 90:
            bool_purpose = True
        else:
            bool_purpose = False
            
        # hardcoded: 20% of all special weapons have detailed descriptions
        i_desc_roll = random.randint(1, 100)
        if i_desc_roll >= 80:
            bool_desc = True
        else:
            bool_desc = False

        if s_weapon_type == 'Ammo':
            wg_s_goods += weapon_select(l_ammo)

        elif s_weapon_type == 'Blades':
            wg_s_goods += weapon_select(l_blades)
        elif s_weapon_type == 'Daggers':
            wg_s_goods += weapon_select(l_daggers)
        elif s_weapon_type == 'Blades Special':
            wg_s_goods += weapon_select(l_blades)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_purpose_type(bool_purpose)
        elif s_weapon_type == 'Daggers Special':
            wg_s_goods += weapon_select(l_daggers)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Hafted':
            wg_s_goods += weapon_select(l_hafted)
        elif s_weapon_type == 'Hafted Special':
            wg_s_goods += weapon_select(l_hafted)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Polearms':
            wg_s_goods += weapon_select(l_polearms)
        elif s_weapon_type == 'Polearms Special':
            wg_s_goods += weapon_select(l_polearms)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Misc':
            wg_s_goods += weapon_select(l_misc)
        elif s_weapon_type == 'Misc Special':
            wg_s_goods += weapon_select(l_misc)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Missile':
            wg_s_goods += weapon_select(l_missile)
        elif s_weapon_type == 'Missile Special':
            wg_s_goods += weapon_select(l_missile)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Guns':
            wg_s_goods += weapon_select(l_guns)
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Tech Blades':
            wg_s_goods += weapon_select(l_tech_blades)
            wg_s_goods += spec_purpose_type(bool_purpose)
        elif s_weapon_type == 'Tech Blades Flawed':
            wg_s_goods += weapon_select(l_tech_blades)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_tech_flaws()
            wg_s_goods += spec_purpose_type(bool_purpose)
        elif s_weapon_type == 'Tech Blades Special':
            wg_s_goods += weapon_select(l_tech_blades)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Lasers':
            wg_s_goods += weapon_select(l_lasers)
        elif s_weapon_type == 'Lasers Flawed':
            wg_s_goods += weapon_select(l_lasers)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_tech_flaws()
            wg_s_goods += spec_purpose_type(bool_purpose)
        elif s_weapon_type == 'Lasers Special':
            wg_s_goods += weapon_select(l_lasers)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_tech_improvements()
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Tech Missiles':
            wg_s_goods += weapon_select(l_tech_missiles)
        elif s_weapon_type == 'Tech Missiles Flawed':
            wg_s_goods += weapon_select(l_tech_missiles)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_tech_flaws()
        elif s_weapon_type == 'Tech Missiles Special':
            wg_s_goods += weapon_select(l_tech_missiles)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_tech_improvements()
            wg_s_goods += spec_purpose_type(bool_purpose)

        elif s_weapon_type == 'Tech Hvy':
            wg_s_goods += weapon_select(l_tech_hvy)
        elif s_weapon_type == 'Tech Hvy Flawed':
            wg_s_goods += weapon_select(l_tech_hvy)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_tech_flaws()
        elif s_weapon_type == 'Tech Hvy Special':
            wg_s_goods += weapon_select(l_tech_hvy)
            wg_s_goods += special_desc(s_weapon_type, bool_desc)
            wg_s_goods += spec_tech_improvements()
            wg_s_goods += spec_purpose_type(bool_purpose)

        # wg_s_goods += (u'\n\t\t[VALUE TO COLLECTOR]: {:,.2f}'.format(
            # s_price) + config.s_dl_brace_dash)
        wg_s_goods += config.s_dl_brace_dash

    return wg_s_goods


def weapon_type_list_creator():
    """
    Select a random weapon from the list and return a Dictionary
    of its information.
    """
    l_weapons = config.csv_to_list('data/weapons/weapon_type.csv')

    # Ammo
    l_ammo = []
    for line in l_weapons:
        if line['Type'] == 'Ammo':
            l_ammo.append(line)
        elif line['Type'] == 'Battery':
            l_ammo.append(line)


    # Blade
    l_blades = []
    for line in l_weapons:
        if line['Type'] == 'Blade':
            l_blades.append(line)

    # Dagger
    l_daggers = []
    for line in l_weapons:
        if line['Type'] == 'Dagger':
            l_daggers.append(line)

    # Hafted
    l_hafted = []
    for line in l_weapons:
        if line['Type'] == 'Hafted':
            l_hafted.append(line)


    # Misc and Special
    l_misc = []
    for line in l_weapons:
        if line['Type'] == 'Misc':
            l_misc.append(line)
        elif line['Type'] == 'Special':
            l_misc.append(line)


    # Launcher and Missile
    l_missile = []
    for line in l_weapons:
        if line['Type'] == 'Launcher':
            l_missile.append(line)
        elif line['Type'] == 'Missile':
            l_missile.append(line)

    # Gun
    l_guns = []
    for line in l_weapons:
        if line['Type'] == 'Gun':
            l_guns.append(line)

    # TL 2: TODO Maybe

    # TL 3: TODO Maybe

    # TL 4: TODO Maybe

    # TL 5: TODO Maybe


    # Tech Blade and Tech Club
    l_tech_blades = []
    for line in l_weapons:
        if line['Type'] == 'Tech Blade':
            l_tech_blades.append(line)
        elif line['Type'] == 'Tech Club':
            l_tech_blades.append(line)

    # Polearms
    l_polearms = []
    for line in l_weapons:
        if line['Type'] == 'Polearms':
            l_polearms.append(line)

    # Laser
    l_lasers = []
    for line in l_weapons:
        if line['Type'] == 'Laser':
            l_lasers.append(line)

    # Tech Missile and Tech Mine
    l_tech_missiles = []
    for line in l_weapons:
        if line['Type'] == 'Tech Missile':
            l_tech_missiles.append(line)
        elif line['Type'] == 'Tech Mine':
            l_tech_missiles.append(line)

    # Tech Hvy Weapon
    l_tech_hvy = []
    for line in l_weapons:
        if line['Type'] == 'Tech Hvy Weapon':
            l_tech_hvy.append(line)

    return (l_ammo,
            l_blades,
            l_hafted,
            l_polearms,
            l_misc,
            l_missile,
            l_guns,
            l_tech_blades,
            l_lasers,
            l_tech_missiles,
            l_tech_hvy,
            l_daggers)


def weapon_description(d_weapon):

    s_weap_desc = ''
    s_weapon = d_weapon['Weapon']
    s_cost = d_weapon['Cost']
    s_weight = d_weapon['Weight']
    s_damage = d_weapon['Damage']
    s_size = d_weapon['Size']
    s_damage_type = d_weapon['Damage Type']
    s_speed = d_weapon['Speed']
    s_rof = d_weapon['ROF']
    s_range = d_weapon['Range']
    s_capacity = d_weapon['Capacity']
    s_type = d_weapon['Type']
    s_tl = d_weapon['Tech Level']
    s_skill = d_weapon['Skill']
    s_attribute = d_weapon['Attribute']
    s_note = (u'[NOTES]: ' + d_weapon['Notes'])

    s_weap_desc += config.s_dl_brace_dash
    s_weap_desc += (u'\t[WEAPON]: {}\n\t\t[Type]: {}'
                    u'\n\t\t[Tech Level]: {}\n\t\t[Base Cost]: {} sp'
                    u'\n\t\t[Size]: {}\n\t\t[Weight]: {}\n'
                    u'\n\t[DAMAGE]: {}\n\t\t[Damage Type]: {}'
                    u'\n\t\t[Speed]: {}\n\t\t[RoF]: {}'
                    u'\n\t\t[Range]: {}\n\t\t[Capacity]: {}'
                    u'\n\n\t[SKILL]: {}\n\t [Attributes]: {}'
                    u''.format(s_weapon,
                               s_type,
                               s_tl,
                               s_cost,
                               s_size,
                               s_weight,
                               s_damage,
                               s_damage_type,
                               s_speed,
                               s_rof,
                               s_range,
                               s_capacity,
                               s_skill,
                               s_attribute))

    s_note_wrap = textwrap.fill(s_note,
                                 initial_indent='    ',
                                 subsequent_indent='    ')
    s_weap_desc += (u'\n' + s_note_wrap + u'')

    return s_weap_desc


def weapon_select(l_weapon):
    """

    """
    s_weapon = ''
    i_rand_select = random.randint(0, len(l_weapon)-1)
    d_weapon = l_weapon[i_rand_select]
    s_weapon += weapon_description(d_weapon)

    return s_weapon


def special_desc(s_w_weap_type, bool_desc=True):
    """

    """
    s_special = ''

    if not bool_desc:
        return s_special
    
    # Blade, Gun, Hafted, Launcher, Misc, Missile, Polearm
    if s_w_weap_type == 'Blade':
        s_special += (u'\n\n\t[BLADE]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HILT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Hilt Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[CROSSGUARD]: ' + spec_crossguard_shape())
        s_special += (u'\n\t[POMMEL]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Pommel Accent]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Blades Special':
        s_special += (u'\n\n\t[BLADE]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HILT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Hilt Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[CROSSGUARD]: ' + spec_crossguard_shape())
        s_special += (u'\n\t[POMMEL]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Pommel Accent]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Daggers':
        s_special += (u'\n\n\t[BLADE]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HILT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Hilt Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[CROSSGUARD]: ' + spec_crossguard_shape())
        s_special += (u'\n\t[POMMEL]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Pommel Accent]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Daggers Special':
        s_special += (u'\n\n\t[BLADE]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HILT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Hilt Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[CROSSGUARD]: ' + spec_crossguard_shape())
        s_special += (u'\n\t[POMMEL]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Pommel Accent]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Guns':
        s_special += (u'\n\n\t[GUN BARREL]: ' + spec_blade_mats())
        s_special += (u'\n\t[GUN HANDLE]: ' + spec_pommel_mats())
        s_special += (u'\n\t[DESIGN MOTIF]: ' + spec_hilt_shape())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Hafted':
        s_special += (u'\n\n\t[HEAD]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_blade_shape())
        s_special += (u'\n\t[HAFT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[END CAP]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Hafted Special':
        s_special += (u'\n\t[HEAD]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_blade_shape())
        s_special += (u'\n\t[HAFT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[END CAP]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Missile':
        s_special += (u'\n\n\t[BODY]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Missile Special':
        s_special += (u'\n\n\t[BODY]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Misc':
        s_special += (u'\n\n\t[HANDLE or BODY]: ' + spec_blade_mats())
    elif s_w_weap_type == 'Misc Special':
        s_special += (u'\n\n\t[HANDLE or BODY]: ' + spec_blade_mats())

    elif s_w_weap_type == 'Polearms':
        s_special += (u'\n\n\t[BLADE]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HAFT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[END CAP]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Polearms Special':
        s_special += (u'\n\n\t[BLADE]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HAFT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[END CAP]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())

    # Laser, Tech Blade or Club, Tech Hvy Weapon, Tech Mine, Tech Missile
    elif s_w_weap_type == 'Tech Blades':
        s_special += (u'\n\n\t[BLADE or HEAD]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HILT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[CROSSGUARD]: ' + spec_crossguard_shape())
        s_special += (u'\n\t[POMMEL]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Tech Blades Flawed':
        s_special += (u'\n\n\t[BLADE or HEAD]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HILT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[CROSSGUARD]: ' + spec_crossguard_shape())
        s_special += (u'\n\t[POMMEL]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Tech Blades Special':
        s_special += (u'\n\n\t[BLADE or HEAD]: ' + spec_blade_mats())
        s_special += (u'\n\t  [Design]: ' + spec_blade_shape())
        s_special += (u'\n\t[HILT]: ' + spec_hilt_mats())
        s_special += (u'\n\t  [Shape]: ' + spec_hilt_shape())
        s_special += (u'\n\t[CROSSGUARD]: ' + spec_crossguard_shape())
        s_special += (u'\n\t[POMMEL]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accent]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Lasers':
        s_special += (u'\n\n\t[BARREL]: ' + spec_blade_mats())
        s_special += (u'\n\t[HANDLE]: ' + spec_pommel_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_hilt_shape())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Lasers Flawed':
        s_special += (u'\n\n\t[BARREL]: ' + spec_blade_mats())
        s_special += (u'\n\t[HANDLE]: ' + spec_pommel_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_hilt_shape())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Lasers Special':
        s_special += (u'\n\n\t[BARREL]: ' + spec_blade_mats())
        s_special += (u'\n\t[HANDLE]: ' + spec_pommel_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_hilt_shape())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Tech Hvy':
        s_special += (u'\n\n\t[BARREL]: ' + spec_blade_mats())
        s_special += (u'\n\t[HANDLE]: ' + spec_pommel_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_hilt_shape())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Tech Hvy Flawed':
        s_special += (u'\n\n\t[BARREL]: ' + spec_blade_mats())
        s_special += (u'\n\t[HANDLE]: ' + spec_pommel_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_hilt_shape())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Tech Hvy Special':
        s_special += (u'\n\n\t[BARREL]: ' + spec_blade_mats())
        s_special += (u'\n\t[HANDLE]: ' + spec_pommel_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_hilt_shape())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())

    elif s_w_weap_type == 'Tech Missiles':
        s_special += (u'\n\n\t[BODY]: ' + spec_blade_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Tech Missiles Flawed':
        s_special += (u'\n\n\t[BODY]: ' + spec_blade_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())
    elif s_w_weap_type == 'Tech Missiles Special':
        s_special += (u'\n\n\t[BODY]: ' + spec_blade_mats())
        s_special += (u'\n\t[DESIGN]: ' + spec_pommel_mats())
        s_special += (u'\n\t  [Accents]: ' + spec_pommel_accent())

    s_special += weirdness()

    return s_special


def weirdness():
    """

    """
    l_weirdness = config.csv_to_list('data/weapons/weapon_weirdness.csv')
    i_weird_roll = random.randint(1, 400)
    s_weirdness = ''

    for line in l_weirdness:
        if i_weird_roll <= int(line['DieRoll']):
            s_weirdness_base = line['Weirdness']
            s_action = line['Action']
            s_description = (u'[SPECIAL QUALITIES]: ' + line['Description'])
            break

    # s_weirdness += (u'\n\t[WEIRDNESS]: {}.'.format(s_weirdness_base))
    s_weirdness += (u'\n\n' + textwrap.fill(s_description,
                                 initial_indent='    ',
                                 subsequent_indent='        '))

    if s_action == 'Roll time':
        s_weirdness += spec_time()
    elif s_action == 'Roll symbols':
        s_weirdness += AF.armor_weirdness_symbols()
    elif s_action == 'Roll substance':
        s_weirdness += AF.armor_weirdness_substance()
    elif s_action == 'Roll sound':
        s_weirdness_obj = 'Weapon'
        s_weirdness += AF.armor_weirdness_noise(s_weirdness_obj)
    elif s_action == 'Roll scent':
        s_weirdness += AF.armor_weirdness_scent()
    elif s_action == 'Roll marks':
        s_weirdness += spec_marks()
    elif s_action == 'Roll emotion':
        s_weirdness += spec_emotions()
    elif s_action == 'Roll display':
        s_weirdness += spec_display()
    elif s_action == 'Roll color and animal':
        s_weirdness += AF.armor_weirdness_color()
        s_weirdness += AF.armor_weirdness_animals()
    elif s_action == 'Roll color':
        s_weirdness += AF.armor_weirdness_color()



    if s_weirdness_base == 'Roll Twice':
        s_weirdness += weirdness()
        s_weirdness += weirdness()

    elif s_weirdness_base == 'Roll Thrice':
        s_weirdness += weirdness()
        s_weirdness += weirdness()
        s_weirdness += weirdness()


    return s_weirdness


def spec_blade_mats():
    """

    """
    l_rare_or_magical_metals = []

    l_gem = []
    l_metals = config.csv_to_list('data/goods/goods_metals.csv')
    l_mats = config.csv_to_list('data/weapons/weapon_blade_material.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_mats_roll = random.randint(1, 400)

    for line in l_mats:
        if i_mats_roll <= int(line['DieRoll']):
            s_mats = (u'{}'.format(line['Material']))
            s_action = line['Action']
            break

    if s_action == 'Roll rare metal':
        for line in l_metals:
            if line['Classification'] == 'Rare':
                l_rare_or_magical_metals.append(line)
        i_rare_or_magic = random.randint(0, len(l_rare_or_magical_metals)-1)
        s_r_or_m = l_rare_or_magical_metals[i_rare_or_magic]['Metal']
        s_r_or_m_desc = (u'[DESCRIPTION]: ' +
                       l_rare_or_magical_metals[i_rare_or_magic]['Description'])
        s_r_or_m_desc = textwrap.fill(s_r_or_m_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u' ({})\n{}'.format(s_r_or_m,
                                          s_r_or_m_desc))

    elif s_action == 'Roll magical metal':
        for line in l_metals:
            if line['Classification'] == 'Magical':
                l_rare_or_magical_metals.append(line)

        i_rare_or_magic = random.randint(0, len(l_rare_or_magical_metals)-1)
        s_r_or_m = l_rare_or_magical_metals[i_rare_or_magic]['Metal']
        s_r_or_m_desc = (u'[DESCRIPTION]: ' +
                       l_rare_or_magical_metals[i_rare_or_magic]['Description'])
        s_r_or_m_desc = textwrap.fill(s_r_or_m_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u' ({})\n{}'.format(s_r_or_m,
                                          s_r_or_m_desc))

    elif s_action == 'Roll animal':
        s_mats += (u'\n\t\t[BONE]: ' + AF.armor_weirdness_animals())

    elif s_action == 'Roll gem':
        l_gem_initial = config.csv_to_list('data/gems/gem_category.csv')
        l_gems = []
        for line in l_gem_initial:
            if line['Category'] != 'Cut':
                l_gem.append(line)

        i_rand_gem = random.randint(0, len(l_gem)-1)

        s_gem_name = l_gem[i_rand_gem]['Name']
        s_gem_desc = (u'[DESCRIPTION]: ' + l_gem[i_rand_gem]['Description'])
        s_gem_wrap = textwrap.fill(s_gem_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')


        s_mats += (u'\n\t\t[Gem]: {}.\n{}'.format(s_gem_name, s_gem_wrap))



    return s_mats


def spec_blade_shape():
    """

    """

    l_shape = config.csv_to_list('data/weapons/weapon_blade_shape.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_shape_roll = random.randint(1, 400)

    for line in l_shape:
        if i_shape_roll <= int(line['DieRoll']):
            s_mats = (u'{}.'.format(line['Type']))
            break


    return s_mats


def spec_crossguard_shape():
    """

    """

    l_shape = config.csv_to_list('data/weapons/weapon_crossguard_shape.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_shape_roll = random.randint(1, 400)

    for line in l_shape:
        if i_shape_roll <= int(line['DieRoll']):
            s_mats = (u'{}.'.format(line['Crossguard Shape']))
            break


    return s_mats


def spec_hilt_mats():
    """

    """

    l_mats = config.csv_to_list('data/weapons/weapon_hilt_material.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_mats_roll = random.randint(1, 400)

    for line in l_mats:
        if i_mats_roll <= int(line['DieRoll']):
            s_mats = (u'{}.'.format(line['Material']))
            s_action = line['Action']
            break

    if s_action == 'Roll Again':
        s_action = 'None'
        i_mats_roll = random.randint(1, 264)
        for line in l_mats:
            if i_mats_roll <= int(line['DieRoll']):
                s_under_mats = line['Material']
                break

        s_mats += (u'\n\t\tUnderneath is {}'.format(s_under_mats))


    return s_mats


def spec_hilt_shape():
    """

    """

    l_shape = config.csv_to_list('data/weapons/weapon_hilt_shape.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_shape_roll = random.randint(1, 400)

    for line in l_shape:
        if i_shape_roll <= int(line['DieRoll']):
            s_mats = (u'{}.'.format(line['Hilt Shape']))
            break


    return s_mats


def spec_pommel_mats():
    """

    """
    l_metals_full = config.csv_to_list('data/goods/goods_metals.csv')
    l_metals = []
    l_mats = config.csv_to_list('data/weapons/weapon_pommel_material.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_mats_roll = random.randint(1, 400)

    for line in l_mats:
        if i_mats_roll <= int(line['DieRoll']):
            s_material = line['Material']
            s_mats = (u'{} '.format(line['Material']))

            break

    if s_material == 'Metal: Rare':
        for line in l_metals_full:
            if line['Classification'] == 'Rare':
                l_metals.append(line)
        i_rare_or_magic = random.randint(0, len(l_metals)-1)
        s_r_or_m = l_metals[i_rare_or_magic]['Metal']
        s_r_or_m_desc = (u'[DESCRIPTION]: ' +
                       l_metals[i_rare_or_magic]['Description'])
        s_r_or_m_desc = textwrap.fill(s_r_or_m_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u'({})\n{}'.format(s_r_or_m,
                                          s_r_or_m_desc))

    elif s_material == 'Metal: Magical':
        for line in l_metals_full:
            if line['Classification'] == 'Magical':
                l_metals.append(line)
        i_rare_or_magic = random.randint(0, len(l_metals)-1)
        s_r_or_m = l_metals[i_rare_or_magic]['Metal']
        s_r_or_m_desc = (u'[DESCRIPTION]: ' +
                       l_metals[i_rare_or_magic]['Description'])
        s_r_or_m_desc = textwrap.fill(s_r_or_m_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u'({})\n{}'.format(s_r_or_m,
                                          s_r_or_m_desc))

    elif s_material == 'Metal: Common':
        for line in l_metals_full:
            if line['Classification'] == 'Common':
                l_metals.append(line)
        i_rare_or_magic = random.randint(0, len(l_metals)-1)
        s_r_or_m = l_metals[i_rare_or_magic]['Metal']
        s_r_or_m_desc = (u'[DESCRIPTION]: ' +
                       l_metals[i_rare_or_magic]['Description'])
        s_r_or_m_desc = textwrap.fill(s_r_or_m_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u'({})\n{}'.format(s_r_or_m,
                                          s_r_or_m_desc))

    return s_mats


def spec_pommel_accent():
    """

    """
    l_rare_or_magical_metals = []

    l_gem = []
    l_metals = config.csv_to_list('data/goods/goods_metals.csv')
    l_mats = config.csv_to_list('data/weapons/weapon_pommel_shape_accent.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_mats_roll = random.randint(1, 400)

    for line in l_mats:
        if i_mats_roll <= int(line['DieRoll']):
            s_mats = (u'{}. '.format(line['Pommel Shape']))
            s_action = line['Action']
            break

    if s_action == 'Roll rare metal':
        for line in l_metals:
            if line['Classification'] == 'Rare':
                l_rare_or_magical_metals.append(line)
        i_rare_or_magic = random.randint(0, len(l_rare_or_magical_metals)-1)
        s_r_or_m = (u'' + l_rare_or_magical_metals[i_rare_or_magic]['Metal'])
        s_r_or_m_desc = (u'[DESCRIPTION]: ' +
                       l_rare_or_magical_metals[i_rare_or_magic]['Description'])
        s_r_or_m_desc = textwrap.fill(s_r_or_m_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u'{}.\n{}'.format(s_r_or_m,
                                          s_r_or_m_desc))

    elif s_action == 'Roll animal':
        s_mats += (u'' + AF.armor_weirdness_animals())

    elif s_action == 'Roll gem':
        l_gem_initial = config.csv_to_list('data/gems/gem_category.csv')
        l_gem = []
        for line in l_gem_initial:
            if line['Category'] != 'Cut':
                l_gem.append(line)

        i_rand_gem = random.randint(0, len(l_gem)-1)

        s_gem_name = l_gem[i_rand_gem]['Name']
        s_gem_desc = (u'[DESCRIPTION]: ' + l_gem[i_rand_gem]['Description'])
        s_gem_wrap = textwrap.fill(s_gem_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')


        s_mats += (u'{}.\n{}'.format(s_gem_name, s_gem_wrap))

    elif s_action == 'Roll symbols':
        s_mats = AF.armor_weirdness_symbols()


    return s_mats


def spec_alignment():
    """

    """
    l_align = config.csv_to_list('data/weapons/weapon_weirdness_alignment.csv')
    i_align_roll = random.randint(0, len(l_align)-1)

    s_alignment = (u'\n\t\t[WORLDVIEW or DESIRE]: {}'.format(l_align[i_align_roll]['Alignments']))

    return s_alignment


def spec_dragons():
    """

    """
    l_dragons = config.csv_to_list('data/weapons/weapon_weirdness_dragons.csv')
    i_dragon_roll = random.randint(0, len(l_dragons)-1)

    s_dragon = (u'\n\t\t[DRAGON TYPE:] {}'.format(l_dragons[i_dragon_roll]['Dragon Type']))

    return s_dragon


def spec_emotions():
    """

    """

    l_emote = config.csv_to_list('data/weapons/weapon_weirdness_emotion.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_emote_roll = random.randint(0, len(l_emote)-1)


    s_emote = (u'  {}.'.format(l_emote[i_emote_roll]['Emotion']))
    s_class = (u'\n\t\t[Type]: {}'.format(l_emote[i_emote_roll]['EARL Classification']))
    s_desc = (u'[DESCRIPTION]: {}'.format(l_emote[i_emote_roll]['Description']))
    s_comb = (u' {}'.format(l_emote[i_emote_roll]['Combat Mechanics']))
    s_socl = (u' {}'.format(l_emote[i_emote_roll]['Social Mechanics']))


    s_desc_wrap = textwrap.fill(s_desc,
                                    initial_indent='        ',
                                    subsequent_indent='              ')
    s_comb_wrap = textwrap.fill(s_comb,
                                    initial_indent='         ',
                                    subsequent_indent='                ')
    s_socl_wrap = textwrap.fill(s_socl,
                                    initial_indent='         ',
                                    subsequent_indent='                ')


    s_mats = (u'\n\t [EMOTION]: {}{}\n\t  {}\n\t  {}\n\t  {}'.format(s_emote,
                                                          s_class,
                                                          s_desc_wrap,
                                                          s_comb_wrap,
                                                          s_socl_wrap))

    return s_mats


def spec_human_colors():
    """

    """

    l_shape = config.csv_to_list('data/weapons/weapon_weirdness_human_color.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_shape_roll = random.randint(1, len(l_shape)-1)

    s_mats = (u'{}.'.format(l_shape[i_shape_roll]['Color']))

    return s_mats


def spec_purpose_type(bool_purpose):
    """
    """
    s_purpose = ''

    if not bool_purpose:
        return s_purpose

    else:
        l_purpose = config.csv_to_list('data/weapons/weapon_weirdness_purpose_type.csv')
        i_purpose_roll = random.randint(1, 400)

        for line in l_purpose:
            if i_purpose_roll <= int(line['DieRoll']):
                s_purp_type = line['Type']
                s_purp_action = line['Action']
                s_description = line['Description']
                break

        s_purpose += (u'\n\t[BASE PURPOSE TYPE]: {}'.format(s_purp_type))
        s_desc = (u'[DESCRIPTION]: {}'.format(s_description))
        s_desc_wrap = textwrap.fill(s_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_purpose += (u'\n{}'.format(s_desc_wrap))

        if s_purp_action == 'Roll alignment':
            s_purpose += spec_alignment()
        elif s_purp_action == 'Roll alignment then Roll 25-260':
            i_basic_power = random.randint(1, 260)
            s_purpose += spec_alignment()
            s_purpose += spec_purpose_generator(i_basic_power)
        elif s_purp_action == 'Roll alignment then Roll 25-400':
            i_power_ability = random.randint(1, 400)
            s_purpose += spec_alignment()
            s_purpose += spec_purpose_generator(i_power_ability)

    return s_purpose


def spec_purpose_generator(i_purpose_roll):
    """
    """
    s_purpose = ''

    l_purpose = config.csv_to_list('data/weapons/weapon_weirdness_purpose.csv')

    for line in l_purpose:
        if i_purpose_roll <= int(line['DieRoll']):
            s_purpose = (u'\n\t  [DESIGNED PURPOSE]: {}'.format(line['Purpose']))
            s_action = line['Action']
            break

    if s_action == 'Roll human color':
        s_purpose += spec_human_colors()

    elif s_action == 'Roll 260':
        i_roll = random.randint(25, 260)
        s_purpose += spec_purpose_generator(i_roll)

    elif s_action == 'Roll 2x':
        i_roll = random.randint(25, 400)
        s_purpose += spec_purpose_generator(i_roll)
        i_roll = random.randint(25, 400)
        s_purpose += spec_purpose_generator(i_roll)

    elif s_action == 'Roll 3x':
        i_roll = random.randint(25, 400)
        s_purpose += spec_purpose_generator(i_roll)
        i_roll = random.randint(25, 400)
        s_purpose += spec_purpose_generator(i_roll)
        i_roll = random.randint(25, 400)
        s_purpose += spec_purpose_generator(i_roll)

    elif s_action == 'Roll 2x 260 and 1x 264-388':
        i_roll = random.randint(25, 260)
        s_purpose += spec_purpose_generator(i_roll)
        i_roll = random.randint(25, 260)
        s_purpose += spec_purpose_generator(i_roll)
        i_roll = random.randint(264, 388)
        s_purpose += spec_purpose_generator(i_roll)



    return s_purpose


def spec_tech_flaws():
    """

    """

    l_shape = config.csv_to_list('data/weapons/weapon_weirdness_tech_flaws.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_shape_roll = random.randint(1, 400)

    for line in l_shape:
        if i_shape_roll <= int(line['DieRoll']):
            s_mats = (u'\n\n\t\t[FLAW]: {}.'.format(line['Flaw']))
            s_desc = (u'[DESCRIPTION]: ' + line['Description'])
            break

    s_desc_wrap = textwrap.fill(s_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')

    s_mats += (u'\n{}'.format(s_desc_wrap))

    return s_mats


def spec_tech_improvements():
    """

    """

    l_shape = config.csv_to_list('data/weapons/weapon_weirdness_tech_improvements.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_shape_roll = random.randint(1, 400)

    for line in l_shape:
        if i_shape_roll <= int(line['DieRoll']):
            s_mats = (u'{}.'.format(line['Improvement']))
            s_desc = (u'[DESCRIPTION]: ' + line['Description'])
            s_actn = line['Action']
            break

    s_desc_wrap = textwrap.fill(s_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
    s_mats += (u'\n\t\t{}'.format(s_desc_wrap))

    if s_actn == 'Roll twice': # 1-200 and Roll 201-380
        s_mats += '\n'
        i_shape_roll = random.randint(1, 200)
        for line in l_shape:
            if i_shape_roll <= int(line['DieRoll']):
                s_mats = (u'{}.'.format(line['Improvement']))
                s_desc = (u'[DESCRIPTION]: ' + line['Description'])
                s_actn = line['Action']
                break

        s_desc_wrap = textwrap.fill(s_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u'\n\t\t{}'.format(s_desc_wrap))

        i_shape_roll = random.randint(201, 380)
        for line in l_shape:
            if i_shape_roll <= int(line['DieRoll']):
                s_mats = (u'{}.'.format(line['Improvement']))
                s_desc = (u'[DESCRIPTION]: ' + line['Description'])
                s_actn = line['Action']
                break

        s_desc_wrap = textwrap.fill(s_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')
        s_mats += (u'\n\t\t{}'.format(s_desc_wrap))


    return s_mats


def spec_time():
    """

    """

    l_shape = config.csv_to_list('data/weapons/weapon_weirdness_time_location.csv')
    s_mats = "I got the '<=' sign wrong way 'round"
    i_shape_roll = random.randint(1, 400)

    for line in l_shape:
        if i_shape_roll <= int(line['DieRoll']):
            s_mats = (u'\n\t  [TIME]: {}.'.format(line['Time or Location']))
            break


    return s_mats


def spec_marks():
    """

    """

    l_marks = config.csv_to_list('data/weapons/weapon_weirdness_marks.csv')
    s_marks = ""
    i_marks_roll = random.randint(1, len(l_marks)-1)

    s_marks += (u'\n\t  [WEAPON MARKS]: Effects last until healed.')
    s_marks += (u'\n\t\t[Mark]: {}.'.format(l_marks[i_marks_roll]['Mark']))
    s_description = (u'[DESCRIPTION]: {}'.format(l_marks[i_marks_roll]['Description']))
    s_desc_wrap = textwrap.fill(s_description,
                                initial_indent='        ',
                                subsequent_indent='        ')
    s_marks += (u'\n' + s_desc_wrap)

    if l_marks[i_marks_roll]['Action'] == 'Roll metal':
        l_metal = config.csv_to_list('data/goods/goods_metals.csv')
        i_metal_roll = random.randint(0, len(l_metal)-1)

        s_metal = l_metal[i_metal_roll]['Metal']
        s_metal_description = (u'[DESCRIPTION]: {}'.format(
                                      l_metal[i_metal_roll]['Description']))
        s_metal_wrap = textwrap.fill(s_metal_description,
                                     initial_indent='        ',
                                     subsequent_indent='        ')
        s_marks += (u'\n\t\t[METAL]: {}'.format(s_metal))
        s_marks += (u'\n{}'.format(s_metal_wrap))

    elif l_marks[i_marks_roll]['Action'] == 'Roll color':
        s_marks += AF.armor_weirdness_color()

    elif l_marks[i_marks_roll]['Action'] == 'Roll gem':
        l_gem_initial = config.csv_to_list('data/gems/gem_category.csv')
        l_gem = []
        for line in l_gem_initial:
            if line['Category'] != 'Cut':
                l_gem.append(line)

        i_rand_gem = random.randint(0, len(l_gem)-1)

        s_gem_name = l_gem[i_rand_gem]['Name']
        s_gem_desc = (u'[DESCRIPTION]: ' + l_gem[i_rand_gem]['Description'])
        s_gem_wrap = textwrap.fill(s_gem_desc,
                                    initial_indent='        ',
                                    subsequent_indent='        ')


        s_marks += (u'\n\t\t[GEM]: {}.\n{}'.format(s_gem_name, s_gem_wrap))

    elif l_marks[i_marks_roll]['Action'] == 'Roll wood':
        l_wood = config.csv_to_list('data/goods/goods_woods.csv')
        i_wood_roll = random.randint(0, len(l_wood)-1)

        s_wood = l_wood[i_wood_roll]['Name']
        s_desc_wood = (u'[DESCRIPTION]: {}'.format(
                                          l_wood[i_wood_roll]['Description']))
        s_wood_wrap = textwrap.fill(s_desc_wood,
                                    initial_indent='        ',
                                    subsequent_indent='        ')

        s_marks += (u'\n\t\t[WOOD]: {}.\n{}'.format(s_wood, s_wood_wrap))


    elif l_marks[i_marks_roll]['Action'] == 'Roll animal':
        s_marks += AF.armor_weirdness_animals()


    elif l_marks[i_marks_roll]['Action'] == 'Roll symbols':
        s_symb = ''
        l_symb = config.csv_to_list('data/armor/armor_weirdness_symbols.csv')
        i_symb = random.randint(0, len(l_symb)-1)

        s_symb += u'\n\t\t[Symbols]: {}'.format(l_symb[i_symb]['Type'])
        s_marks += s_symb
        s_desc = u'[DESCRIPTION]: {}'.format(l_symb[i_symb]['Marked Description'])
        s_wrap = textwrap.fill(s_desc, initial_indent=
                                    '        ', subsequent_indent=
                                    '        ')

        s_marks += u'\n\t\t' + s_wrap


    return s_marks



    return s_marks


def spec_display():
    """

    """
    l_display = config.csv_to_list('data/weapons/weapon_weirdness_visual.csv')
    i_display_roll = random.randint(1, 400)
    s_display = ''

    for line in l_display:
        if i_display_roll <= int(line['DieRoll']):
            s_visual = line['Visual']
            s_action = line['Action']
            s_description = (u'[DESCRIPTION]: {}'.format(line['Description']))
            break

    s_display += (u'\n\t  [VISUAL DISPLAY]: {}'.format(s_visual))
    s_desc_wrap = textwrap.fill(s_description,
                                initial_indent='        ',
                                subsequent_indent='        ')
    s_display += u'\n\t\t' + s_desc_wrap

    if s_action == 'Roll color':
        s_display += AF.armor_weirdness_color()

    return s_display