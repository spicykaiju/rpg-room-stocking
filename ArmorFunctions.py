#! /usr/bin/env python
""" Functions on generating & returning a string of armor information.

"""

# import statments
import random
import config
import textwrap
from GemFunctions import gem_main
import MagicItemsFunctions as MIF

__author__ = 'Roger E. Burgess, III'
__copyright__ = "Copyright 2014, Red Flag & Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or later"
__version__ = "0.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"



def armor_generation(d_line=None, f_split=None):
    if d_line is None:
        d_line = {'FLOAT: Value Low':4, 'FLOAT: Value High':10000}

    if f_split is None:
        f_split = 101.0


    ag_s_goods = ''

    f_goods_value_low = float(d_line['FLOAT: Value Low'])
    f_goods_value_high = float(d_line['FLOAT: Value High'])
    f__rand_baseprice = random.uniform(f_goods_value_low, f_goods_value_high)

    l_shield_or_suit = shield_or_suit()
    ag_s_goods += armor_suit(l_shield_or_suit,
                             f_split,
                             f_goods_value_low,
                             f_goods_value_high,
                             f__rand_baseprice)

    return ag_s_goods


def shield_or_suit():
    i_shield_or_suit = random.randint(1, 100)

    if i_shield_or_suit <= 60:
        l_armor_choice = config.csv_to_list('data/armor/armor_shield_type.csv')

    else:
        l_armor_choice = config.csv_to_list('data/armor/armor_type.csv')

    return l_armor_choice


def armor_suit(l_armor,
               f_split=0,
               f_goods_value_low=0,
               f_goods_value_high=0,
               f_as_rand_base_price=0):

    if f_split == 0:
        f_split = 100
    if f_goods_value_low == 0:
        f_goods_value_low = 99
    if f_goods_value_high == 0:
        f_goods_value_high = 200
    if f_as_rand_base_price == 0:
        f_as_rand_base_price = 200
    if l_armor is None:
        l_armor = config.csv_to_list('data/armor/armor_type.csv')

    s_as_armor = ''
    i_armor_roll = random.randint(1, 400)

    for line in l_armor:

        if i_armor_roll <= int(line['DieRoll']):

            s_armor = line['Armor']
            s_standard_base_cost = line['Base Cost (in sp)']
            s_armor_weight = line['Weight']
            s_ADDAC = line['AC: 1e/2e']
            s_BXAC = line['AC: B/X']
            s_AsAC = line['AC: Ascnd']
            s_ACAC = line['AC: ACKS']
            s_DPnt = line['Dp']
            s_Slash = line['S']
            s_Pierc = line['P']
            s_Bludg = line['B']
            s_Mats = line['Material']
            s_Desc = line['Description']
            break

    s_desc_temp = u'[DESCRIPTION]: {}'.format(s_Desc)
    s_desc_wrap = textwrap.fill(s_desc_temp, initial_indent=
                                '        ', subsequent_indent=
                                '        ') + '\n'

    f_as_rand_base_price += int(s_standard_base_cost)
    s_as_armor += ('\n\t\t[ARMOR]: {}'
                   '\n{}'
                   '\n\t\t[BASE COST]: {}'
                   '\n\t\t[WEIGHT]: {}'
                   '\n\t\t[AC] '
                   '\n\t\t\t|1e/2e: {}'
                   '\n\t\t\t|<BX>: <{}>'
                   '\n\t\t\t|[Ascending]: [{}]'
                   '\n\t\t\t|(ACKS): ({})'
                   '\n\t\t[AC Bonuses]: '
                   '[S: {}] [P: {}] [B: {}]'
                   '\n\t\t[Damage Points]: {}\n'
                   '').format(s_armor, s_desc_wrap, s_standard_base_cost, s_armor_weight,
                              s_ADDAC, s_BXAC, s_AsAC, s_ACAC,
                              s_Slash, s_Pierc, s_Bludg, s_DPnt)
    # We have three money values;
    # 1. Value of the treasure split: f_split.
    # 2. Base cost of the armor: s_base_cost
    # 3, Cost modifier of the armor: f_as_rand_base_price

    # First, make sure that the goods value is at least equal to the base
    # cost of the armor we've selected.
    if f_as_rand_base_price < float(s_standard_base_cost):
        f_as_rand_base_price = float(s_standard_base_cost)



    s_as_armor += base_armor_generation(s_Mats, f_as_rand_base_price)

    if f_goods_value_low > f_split:
        s_goods_temp = (u'[VALUE]: Really?  This garbage might'
                        u' be worth {:,.2f} sp to an '
                        u'Bone Man.  A really desperate '
                        u'Bone Man.').format(f_split)

        s_as_armor += textwrap.fill(s_goods_temp, initial_indent=
                                '        ', subsequent_indent=
                                '        ') + '\n'
    else:
        s_as_armor += u'\n\t\t[Estimated value]: {:,.2f}.\n'.format(
                                                            f_as_rand_base_price)

    return s_as_armor


def base_armor_generation(s_material, f_bag_rand_base_price):
    s_base_armor_gen = ''

    if s_material == 'Fabric':
        s_base_armor_gen += generate_armor_fabric(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    elif s_material == 'Leather':
        s_base_armor_gen += generate_armor_leather(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    elif s_material == 'Scale':
        s_base_armor_gen += generate_armor_scale(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    elif s_material == 'Fur':
        s_base_armor_gen += generate_armor_fur(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    elif s_material == 'Metal':
        s_base_armor_gen += generate_armor_metal(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    elif s_material == 'Bronze':
        s_base_armor_gen += generate_armor_metal(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    elif s_material == 'Wood':
        s_base_armor_gen += generate_armor_wooden(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    elif s_material == 'Metal and wood':
        s_base_armor_gen += generate_armor_wood_and_metal(s_material,
                                                  400,
                                                  f_bag_rand_base_price)


    return s_base_armor_gen


def armor_weirdness(i_armor_weird_roll, f_aw_rand_base_price):
    l_armor_weird = config.csv_to_list('data/armor/armor_weirdness.csv')
    s_weirdness = u'\n\t\t[SPECIAL QUALITIES]: '
    i_armor_random = random.randint(1, i_armor_weird_roll)
    f_true_value = f_aw_rand_base_price

    l_weirdness_type = {}
    for line in l_armor_weird:
        if i_armor_random <= int(line['DieRoll']):
            l_weirdness_type = line
            break
    i_value_mod = int(l_weirdness_type['Cost Mod'])

    if l_weirdness_type['Action'] == 'None':
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll color':
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        s_weirdness += armor_weirdness_color()
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll scent':
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        s_weirdness += armor_weirdness_scent()
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll noise':
        s_obj_type = 'Armor'
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        s_weirdness += armor_weirdness_noise(s_obj_type)
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll animal':
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        s_weirdness += armor_weirdness_animals()
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll protrusion':
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        s_weirdness += armor_weirdness_protrusions(f_aw_rand_base_price)
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll substance':
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        s_weirdness += armor_weirdness_substance()
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll symbols':
        s_weirdness += u'\n\t\t[QUALITY]: {}.'.format(l_weirdness_type['Weirdness'])
        s_weirdness += armor_weirdness_symbols()
        f_true_value = f_aw_rand_base_price * i_value_mod

    elif l_weirdness_type['Action'] == 'Roll2':
        s_weirdness += armor_weirdness(400, f_true_value)
        s_weirdness += armor_weirdness(400, f_true_value)

    elif l_weirdness_type['Action'] == 'Roll3':
        s_weirdness += armor_weirdness(400, f_true_value)
        s_weirdness += armor_weirdness(400, f_true_value)
        s_weirdness += armor_weirdness(400, f_true_value)

    s_weirdness += (u'\n\t\t[Value, if Special Qualities Known]: {:,.2f}'.format(f_true_value))

    return s_weirdness


def armor_weirdness_color():
    s_color = ''
    l_color = config.csv_to_list('data/armor/armor_weirdness_color.csv')
    i_color = random.randint(0, len(l_color)-1)

    s_color += u'\n\t\t[Color]: {}'.format(l_color[i_color]['Color'])

    return s_color


def armor_weirdness_noise(s_obj):
    s_noise = ''
    l_noise_base = config.csv_to_list('data/armor/armor_weirdness_noise.csv')


    l_noise = []
    if s_obj == 'Armor':
        for line in l_noise_base:
            if line['Application'] == 'Armor':
                l_noise.append(line)
            elif line['Application'] == 'Both':
                l_noise.append(line)

    else:
        for line in l_noise_base:
            if line['Application'] == 'Weapon':
                l_noise.append(line)
            elif line['Application'] == 'Both':
                l_noise.append(line)

    i_noise = random.randint(0, len(l_noise)-1)

    s_noise += u'\n\t\t\t[Noise]: {}'.format(l_noise[i_noise]['Type'])
    s_noise += u'\n\t\t\t[Volume]: {}'.format(l_noise[i_noise]['Volume'])
    s_effect = u'[Effect]: ' + l_noise[i_noise]['Effect']
    textwrap.fill(s_effect,
                  initial_indent='            ',
                  subsequent_indent='            ')
    s_noise += (u'\n{}'.format(s_effect))

    return s_noise


def armor_weirdness_protrusions(f_awp_split_value):

    s_protrusion = ''
    l_protrusion = config.csv_to_list('data/armor/armor_weirdness_protrusions.csv')
    i_protrusion = random.randint(0, len(l_protrusion)-1)

    s_protrusion += u'\n\t\t[Mystical Protrusions]: {}'.format(l_protrusion[i_protrusion]['Type'])

    if l_protrusion[i_protrusion]['Action'] == 'Roll gem':
        s_protrusion += u'\n\t\t[Gemstones]: '
        s_protrusion += gem_main(f_awp_split_value)


    elif l_protrusion[i_protrusion]['Action'] == 'Roll color':
        s_protrusion += armor_weirdness_color()

    elif l_protrusion[i_protrusion]['Action'] == 'Roll feathers':

        l_animal = config.csv_to_list('data/armor/armor_weirdness_animal.csv')
        i_animal = random.randint(1, 400)

        for line in l_animal:
            if i_animal <= int(line['DieRoll']):
                l_selected_animal_type = line
                break

        if l_selected_animal_type['Type'] == 'Common':
            l_common = config.csv_to_list('data/goods/goods_common_animals.csv')
            l_common_feathers = []
            for line in l_common:
                if line['Hide'] == 'Feathers':
                    l_common_feathers.append(line)

            i_common = random.randint(0, len(l_common)-1)
            s_protrusion += u'\n\t\t[Motif]: Decorated with the feathers of the '
            s_protrusion += l_common_feathers[i_common]['Animal']

        elif l_selected_animal_type['Type'] == 'Dinosaur':
            l_dino = config.csv_to_list('data/goods/goods_dinosaurs.csv')
            l_dino_feathers = []
            for line in l_dino:
                if line['Resources1'] == 'Feathers':
                    l_dino_feathers.append(line)
                elif line['Resources2'] == 'Feathers':
                    l_dino_feathers.append(line)
                elif line['Resources2'] == 'Feathers':
                    l_dino_feathers.append(line)


            i_dino = random.randint(0, len(l_dino)-1)
            s_protrusion += u'\n\t\t[Motif]: Decorated with the feathers of the '
            s_protrusion += l_dino_feathers[i_dino]['Dinosaur']

        elif l_selected_animal_type['Type'] == 'Rare':
            l_rare = config.csv_to_list('data/goods/goods_rare_animals.csv')
            l_rare_feathers = []
            for line in l_rare:
                if line['Hide'] == 'Feathers':
                    l_rare_feathers.append(line)

            i_rare = random.randint(0, len(l_rare)-1)
            s_protrusion += u'\n\t\t[Motif]: Decorated with the feathers of the '
            s_protrusion += l_rare_feathers[i_rare]['Animal']

        elif l_selected_animal_type['Type'] == 'Magical':
            l_magic = config.csv_to_list('data/goods/goods_magical_animals.csv')
            l_magic_feathers = []
            for line in l_magic:
                if line['Hide'] == 'Feathers':
                    l_magic_feathers.append(line)

            i_magic = random.randint(0, len(l_magic)-1)
            s_protrusion += u'\n\t\t[Motif]: Decorated with the feathers of the '
            s_protrusion += l_magic_feathers[i_magic]['Animal']

    return s_protrusion


def armor_weirdness_scent():
    s_scent = ''
    l_scent = config.csv_to_list('data/armor/armor_weirdness_scent.csv')
    i_scent = random.randint(0, len(l_scent)-1)

    s_scent += u'\n\t\t[Scent]: {}'.format(l_scent[i_scent]['Type'])

    return s_scent


def armor_weirdness_substance():
    s_subst = ''
    l_subst = config.csv_to_list('data/armor/armor_weirdness_substance.csv')
    i_subst = random.randint(0, len(l_subst)-1)

    s_subst += u'\n\t\t[Substance Exuded]: {}'.format(l_subst[i_subst]['Type'])
    s_subst_desc = (u'\n[Description]: {}'.format(l_subst[i_subst]['Description']))

    s_wrap = textwrap.fill(s_subst_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')

    s_subst += u'\n' + s_wrap


    return s_subst


def armor_weirdness_symbols():
    s_symb = ''
    l_symb = config.csv_to_list('data/armor/armor_weirdness_symbols.csv')
    i_symb = random.randint(0, len(l_symb)-1)

    s_symb += u'\n\t\t[Symbols]: {}'.format(l_symb[i_symb]['Type'])
    s_desc = u'[DESCRIPTION]: {}'.format(l_symb[i_symb]['Description'])
    s_wrap = textwrap.fill(s_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')

    s_symb += u'\n' + s_wrap


    return s_symb


def armor_weirdness_animals():
    s_animal = ''
    l_animal = config.csv_to_list('data/armor/armor_weirdness_animal.csv')
    i_animal = random.randint(1, 400)

    for line in l_animal:
        if i_animal <= int(line['DieRoll']):
            l_selected_animal_type = line
            break

    if l_selected_animal_type['Type'] == 'Common':
        l_common = config.csv_to_list('data/goods/goods_common_animals.csv')
        i_common = random.randint(0, len(l_common)-1)
        s_animal += u'\n\t\t[Animal]: '
        s_animal += l_common[i_common]['Animal']

    elif l_selected_animal_type['Type'] == 'Dinosaur':
        l_dino = config.csv_to_list('data/goods/goods_dinosaurs.csv')
        i_dino = random.randint(0, len(l_dino)-1)
        s_animal += u'\n\t\t[Animal]: '
        s_animal += l_dino[i_dino]['Dinosaur']

    elif l_selected_animal_type['Type'] == 'Rare':
        l_rare = config.csv_to_list('data/goods/goods_rare_animals.csv')
        i_rare = random.randint(0, len(l_rare)-1)
        s_animal += u'\n\t\t[Animal]: '
        s_animal += l_rare[i_rare]['Animal']

    elif l_selected_animal_type['Type'] == 'Magical':
        l_magic = config.csv_to_list('data/goods/goods_magical_animals.csv')
        i_magic = random.randint(0, len(l_magic)-1)
        s_animal += u'\n\t\t[Animal]: '
        s_animal += l_magic[i_magic]['Animal']

    return s_animal


def generate_armor_leather(s_mat,
                             i_dieroll,
                             f_gal_rand_base_price):

    s_leather_type = ''
    s_leather_armor = ''
    l_leather = config.csv_to_list('data/armor/armor_leather_type.csv')

    i_gal_dieroll = random.randint(1, i_dieroll)

    for line in l_leather:
        if i_gal_dieroll <= int(line['DieRoll']):
            s_leather_type += line['Type']
            break


    if s_leather_type == 'Common':
        l_common_leather = []
        l_common = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Leather':
                l_common_leather.append(line)

        i_common_leather_roll = random.randint(0, len(l_common_leather)-1)
        s_animal = l_common_leather[i_common_leather_roll]['Animal']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_animal)

    elif s_leather_type == 'Dinosaur':
        l_dinosaur_leather = []
        l_dinosaur = config.csv_to_list('data/goods/goods_dinosaurs.csv')

        for line in l_dinosaur:
            if line['Resources1'] == 'Leather':
                l_dinosaur_leather.append(line)

        i_dinosaur_leather_roll = random.randint(0, len(l_dinosaur_leather)-1)
        s_dinosaur = l_dinosaur_leather[i_dinosaur_leather_roll]['Dinosaur']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_dinosaur)

        if l_dinosaur_leather[i_dinosaur_leather_roll]['Resources1'] == 'Feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers.'.format(s_dinosaur)
        elif l_dinosaur_leather[i_dinosaur_leather_roll]['Resources2'] == 'Feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers.'.format(s_dinosaur)
        elif l_dinosaur_leather[i_dinosaur_leather_roll]['Resources3'] == 'Feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers.'.format(s_dinosaur)

    elif s_leather_type == 'Rare':
        l_rare_leather = []
        l_common = config.csv_to_list('data/goods/goods_rare_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Leather':
                l_rare_leather.append(line)

        i_rare_leather_roll = random.randint(0, len(l_rare_leather)-1)
        s_rare_animal = l_rare_leather[i_rare_leather_roll]['Animal']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_rare_animal)

    elif s_leather_type == 'Magical':
        l_magical_leather = []
        l_common = config.csv_to_list('data/goods/goods_magical_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Leather':
                l_magical_leather.append(line)
            elif line['Hide'] == 'Leather and feathers':
                l_magical_leather.append(line)

        i_magical_leather_roll = random.randint(0, len(l_magical_leather)-1)
        s_magical_animal = l_magical_leather[i_magical_leather_roll]['Animal']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_magical_animal)

        if l_magical_leather[i_magical_leather_roll]['Hide'] == 'Leather and feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers'.format(s_magical_animal)

    elif s_leather_type == 'Common Special':
        l_common_leather = []
        l_common = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Leather':
                l_common_leather.append(line)

        i_common_leather_roll = random.randint(0, len(l_common_leather)-1)
        s_animal = l_common_leather[i_common_leather_roll]['Animal']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_animal)
        s_leather_armor += armor_weirdness(400, f_gal_rand_base_price)

    elif s_leather_type == 'Dinosaur Special':
        l_dinosaur_leather = []
        l_dinosaur = config.csv_to_list('data/goods/goods_dinosaurs.csv')

        for line in l_dinosaur:
            if line['Resources1'] == 'Leather':
                l_dinosaur_leather.append(line)

        i_dinosaur_leather_roll = random.randint(0, len(l_dinosaur_leather)-1)
        s_dinosaur = l_dinosaur_leather[i_dinosaur_leather_roll]['Dinosaur']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_dinosaur)

        if l_dinosaur_leather[i_dinosaur_leather_roll]['Resources1'] == 'Feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers.'.format(s_dinosaur)
        elif l_dinosaur_leather[i_dinosaur_leather_roll]['Resources2'] == 'Feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers.'.format(s_dinosaur)
        elif l_dinosaur_leather[i_dinosaur_leather_roll]['Resources3'] == 'Feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers.'.format(s_dinosaur)

        s_leather_armor += armor_weirdness(400, f_gal_rand_base_price)

    elif s_leather_type == 'Rare Special':
        l_rare_leather = []
        l_common = config.csv_to_list('data/goods/goods_rare_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Leather':
                l_rare_leather.append(line)

        i_rare_leather_roll = random.randint(0, len(l_rare_leather)-1)
        s_rare_animal = l_rare_leather[i_rare_leather_roll]['Animal']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_rare_animal)

        s_leather_armor += armor_weirdness(400, f_gal_rand_base_price)

    elif s_leather_type == 'Magical Special':
        l_magical_leather = []
        l_common = config.csv_to_list('data/goods/goods_magical_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Leather':
                l_magical_leather.append(line)
            elif line['Hide'] == 'Leather and feathers':
                l_magical_leather.append(line)

        i_magical_leather_roll = random.randint(0, len(l_magical_leather)-1)
        s_magical_animal = l_magical_leather[i_magical_leather_roll]['Animal']
        s_leather_armor += u'\n\t\t[LEATHER]: {}'.format(s_magical_animal)

        if l_magical_leather[i_magical_leather_roll]['Hide'] == 'Leather and feathers':
            s_leather_armor += u'\n\t\t[DECORATED WITH]: {} feathers'.format(s_magical_animal)

        s_leather_armor += armor_weirdness(400, f_gal_rand_base_price)


    return s_leather_armor


def generate_armor_metal(s_mat, 
                           i_dieroll, 
                           f_gam_rand_base_price):


    s_metal_type = ''
    s_metal_armor = ''
    l_metal = config.csv_to_list('data/armor/armor_metal_type.csv')
    i_gam_dieroll = random.randint(1, i_dieroll)

    for line in l_metal:
        if i_gam_dieroll <= int(line['DieRoll']):
            s_metal_type += line['Type']
            break


    if s_metal_type == 'Common':
        l_common_metal = []
        l_common = config.csv_to_list('data/goods/goods_metals.csv')

        for line in l_common:
            if line['Classification'] == 'Common':
                l_common_metal.append(line)

        i_common_metal_roll = random.randint(0, len(l_common_metal)-1)
        s_metal = l_common_metal[i_common_metal_roll]['Metal']
        s_metal_armor += u'\n\t\t[METAL]: {}'.format(s_metal)
        s_metal_desc = u'[DESCRIPTION]: {}'.format(l_common_metal[i_common_metal_roll]['Description'])
        s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_metal_armor += u'\n\t\t' + s_metal_wrap


    if s_metal_type == 'Rare':
        l_rare_metal = []
        l_rare = config.csv_to_list('data/goods/goods_metals.csv')

        for line in l_rare:
            if line['Classification'] == 'Rare':
                l_rare_metal.append(line)

        i_rare_metal_roll = random.randint(0, len(l_rare_metal)-1)
        s_metal = l_rare_metal[i_rare_metal_roll]['Metal']
        s_metal_armor += u'\n\t\t[METAL]: {}'.format(s_metal)
        s_metal_desc = u'[DESCRIPTION]: {}'.format(l_rare_metal[i_rare_metal_roll]['Description'])
        s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_metal_armor += u'\n\t\t' + s_metal_wrap


    if s_metal_type == 'Magical':
        l_magical_metal = []
        l_magical = config.csv_to_list('data/goods/goods_metals.csv')

        for line in l_magical:
            if line['Classification'] == 'Common':
                l_magical_metal.append(line)

        i_magical_metal_roll = random.randint(0, len(l_magical_metal)-1)
        s_metal = l_magical_metal[i_magical_metal_roll]['Metal']
        s_metal_armor += u'\n\t\t[METAL]: {}'.format(s_metal)
        s_metal_desc = u'[DESCRIPTION]: {}'.format(l_magical_metal[i_magical_metal_roll]['Description'])
        s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')

        s_metal_armor += u'\n\t\t' + s_metal_wrap

    if s_metal_type == 'Common Special':
        l_common_metal = []
        l_common = config.csv_to_list('data/goods/goods_metals.csv')

        for line in l_common:
            if line['Classification'] == 'Common':
                l_common_metal.append(line)

        i_common_metal_roll = random.randint(0, len(l_common_metal)-1)
        s_metal = l_common_metal[i_common_metal_roll]['Metal']
        s_metal_armor += u'\n\t\t[METAL]: {}'.format(s_metal)
        s_metal_desc = u'[DESCRIPTION]: {}'.format(l_common_metal[i_common_metal_roll]['Description'])
        s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_metal_armor += u'\n\t\t' + s_metal_wrap
        s_metal_armor += armor_weirdness(400, f_gam_rand_base_price)


    if s_metal_type == 'Rare Special':
        l_rare_metal = []
        l_rare = config.csv_to_list('data/goods/goods_metals.csv')

        for line in l_rare:
            if line['Classification'] == 'Rare':
                l_rare_metal.append(line)

        i_rare_metal_roll = random.randint(0, len(l_rare_metal)-1)
        s_metal = l_rare_metal[i_rare_metal_roll]['Metal']
        s_metal_armor += u'\n\t\t[METAL]: {}'.format(s_metal)
        s_metal_desc = u'[DESCRIPTION]: {}'.format(l_rare_metal[i_rare_metal_roll]['Description'])
        s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_metal_armor += u'\n\t\t' + s_metal_wrap
        s_metal_armor += armor_weirdness(400, f_gam_rand_base_price)

    if s_metal_type == 'Magical Special':
        l_magical_metal = []
        l_magical = config.csv_to_list('data/goods/goods_metals.csv')

        for line in l_magical:
            if line['Classification'] == 'Common':
                l_magical_metal.append(line)

        i_magical_metal_roll = random.randint(0, len(l_magical_metal)-1)
        s_metal = l_magical_metal[i_magical_metal_roll]['Metal']
        s_metal_armor += u'\n\t\t[METAL]: {}'.format(s_metal)
        s_metal_desc = u'[DESCRIPTION]: {}'.format(l_magical_metal[i_magical_metal_roll]['Description'])
        s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_metal_armor += u'\n\t\t' + s_metal_wrap
        s_metal_armor += armor_weirdness(400, f_gam_rand_base_price)


    return s_metal_armor


def generate_armor_wooden(s_mat = '',
                            i_dieroll=400,
                            f_gaw_rand_base_price=150):
    """
    Generates wooden armor
    if mat == Wood
    """

    s_wood_type = ''
    s_wood_armor = ''
    l_wood = config.csv_to_list('data/armor/armor_wood_type.csv')
    i_gaw_dieroll = random.randint(1, i_dieroll)

    for line in l_wood:
        if i_gaw_dieroll <= int(line['DieRoll']):
            s_wood_type += line['Type']
            break


    if s_wood_type == 'Hardwood or Ivory':
        l_hardwood = []
        l_hardwood_listing = config.csv_to_list('data/goods/goods_woods.csv')

        for line in l_hardwood_listing:
            if line['Wood Type'] == 'Hardwood':
                l_hardwood.append(line)
            elif line['Wood Type'] == 'Ivory':
                l_hardwood.append(line)

        i_hardwood_roll = random.randint(0, len(l_hardwood)-1)
        s_wood_type = l_hardwood[i_hardwood_roll]['Name']
        s_wood_armor += u'\n\t\t[WOOD or IVORY]: {}'.format(s_wood_type)
        s_wood_desc = u'[DESCRIPTION]: {}'.format(l_hardwood[i_hardwood_roll]['Description'])
        s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_wood_armor += u'\n\t\t' + s_wood_wrap

    if s_wood_type == 'Magical':
        l_magic_wood = []
        l_magic_wood_listing = config.csv_to_list('data/goods/goods_woods.csv')

        for line in l_magic_wood_listing:
            if line['Wood Type'] == 'Magical':
                l_magic_wood.append(line)


        i_magic_wood_roll = random.randint(0, len(l_magic_wood)-1)
        s_wood_type = l_magic_wood[i_magic_wood_roll]['Name']
        s_wood_armor += u'\n\t\t[MAGICAL WOOD]: {}'.format(s_wood_type)
        s_wood_desc = u'[DESCRIPTION]: {}'.format(l_magic_wood[i_magic_wood_roll]['Description'])
        s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_wood_armor += u'\n\t\t' + s_wood_wrap

    if s_wood_type == 'Hardwood Special':
        l_hardwood = []
        l_hardwood_listing = config.csv_to_list('data/goods/goods_woods.csv')

        for line in l_hardwood_listing:
            if line['Wood Type'] == 'Hardwood':
                l_hardwood.append(line)
            elif line['Wood Type'] == 'Ivory':
                l_hardwood.append(line)
            elif line['Wood Type'] == 'Softwood':
                l_hardwood.append(line)

        i_hardwood_roll = random.randint(0, len(l_hardwood)-1)
        s_wood_type = l_hardwood[i_hardwood_roll]['Name']
        s_wood_armor += u'\n\t\t[WOOD or IVORY]: {}'.format(s_wood_type)
        s_wood_desc = u'[DESCRIPTION]: {}'.format(l_hardwood[i_hardwood_roll]['Description'])
        s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_wood_armor += u'\n\t\t' + s_wood_wrap
        s_wood_armor += armor_weirdness(400, f_gaw_rand_base_price)

    if s_wood_type == 'Magical Special':
        l_magic_wood = []
        l_magic_wood_listing = config.csv_to_list('data/goods/goods_woods.csv')

        for line in l_magic_wood_listing:
            if line['Wood Type'] == 'Magical':
                l_magic_wood.append(line)


        i_magic_wood_roll = random.randint(0, len(l_magic_wood)-1)
        s_wood_type = l_magic_wood[i_magic_wood_roll]['Name']
        s_wood_armor += u'\n\t\t[MAGICAL WOOD]: {}'.format(s_wood_type)
        s_wood_desc = u'[DESCRIPTION]: {}'.format(l_magic_wood[i_magic_wood_roll]['Description'])
        s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '        ', subsequent_indent=
                                '        ')
        s_wood_armor += u'\n\t\t' + s_wood_wrap
        s_wood_armor += armor_weirdness(400, f_gaw_rand_base_price)


    return s_wood_armor

def generate_armor_fabric(s_mat,
                            i_dieroll,
                            f_gaf_rand_base_price):

    s_armor_fabric = ''
    s_armor_fabric += u'\n\t\t[FABRIC]: Sturdy linen.'
    i_armor_fabric_roll = random.randint(1, i_dieroll)

    if i_armor_fabric_roll >= 360:
        s_armor_fabric += armor_weirdness(400, f_gaf_rand_base_price)

    return s_armor_fabric

def generate_armor_scale(s_mat,
                           i_dieroll,
                           f_gas_rand_base_price):

    s_scale_type = ''
    s_scale_armor = ''
    l_scale = config.csv_to_list('data/armor/armor_scale_type.csv')
    i_gas_dieroll = random.randint(1, i_dieroll)

    for line in l_scale:
        if i_gas_dieroll <= int(line['DieRoll']):
            s_scale_type += line['Type']
            break

    if s_scale_type == 'Common':
        l_common_scale = []
        l_common = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Scales':
                l_common_scale.append(line)

        i_common_scale_roll = random.randint(0, len(l_common_scale)-1)
        s_animal = l_common_scale[i_common_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)

    elif s_scale_type == 'Dinosaur':
        l_dino_scale = []
        l_dino = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_dino:
            if line['Resources1'] == 'Scales':
                l_dino_scale.append(line)
            elif line['Resources2'] == 'Scales':
                l_dino_scale.append(line)

        i_dino_scale_roll = random.randint(0, len(l_dino_scale)-1)
        s_animal = l_dino_scale[i_dino_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)

    elif s_scale_type == 'Rare':
        l_rare_scale = []
        l_rare = config.csv_to_list('data/goods/goods_Rare_animals.csv')

        for line in l_rare:
            if line['Hide'] == 'Scales':
                l_rare_scale.append(line)

        i_rare_scale_roll = random.randint(0, len(l_rare_scale)-1)
        s_animal = l_rare_scale[i_rare_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)

    elif s_scale_type == 'Magical':
        l_magical_scale = []
        l_magical = config.csv_to_list('data/goods/goods_magical_animals.csv')

        for line in l_magical:
            if line['Hide'] == 'Scales':
                l_magical_scale.append(line)

        i_magical_scale_roll = random.randint(0, len(l_magical_scale)-1)
        s_animal = l_magical_scale[i_magical_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)

    elif s_scale_type == 'Common Special':
        l_common_scale = []
        l_common = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Scales':
                l_common_scale.append(line)

        i_common_scale_roll = random.randint(0, len(l_common_scale)-1)
        s_animal = l_common_scale[i_common_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)
        s_scale_armor += armor_weirdness(400, f_gas_rand_base_price)

    elif s_scale_type == 'Dinosaur Special':
        l_dino_scale = []
        l_dino = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_dino:
            if line['Resources1'] == 'Scales':
                l_dino_scale.append(line)
            elif line['Resources2'] == 'Scales':
                l_dino_scale.append(line)

        i_dino_scale_roll = random.randint(0, len(l_dino_scale)-1)
        s_animal = l_dino_scale[i_dino_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)
        s_scale_armor += armor_weirdness(400, f_gas_rand_base_price)

    elif s_scale_type == 'Rare Special':
        l_rare_scale = []
        l_rare = config.csv_to_list('data/goods/goods_Rare_animals.csv')

        for line in l_rare:
            if line['Hide'] == 'Scales':
                l_rare_scale.append(line)

        i_rare_scale_roll = random.randint(0, len(l_rare_scale)-1)
        s_animal = l_rare_scale[i_rare_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)
        s_scale_armor += armor_weirdness(400, f_gas_rand_base_price)

    elif s_scale_type == 'Magical Special':
        l_magical_scale = []
        l_magical = config.csv_to_list('data/goods/goods_magical_animals.csv')

        for line in l_magical:
            if line['Hide'] == 'Scales':
                l_magical_scale.append(line)

        i_magical_scale_roll = random.randint(0, len(l_magical_scale)-1)
        s_animal = l_magical_scale[i_magical_scale_roll]['Animal']
        s_scale_armor += u'\n\t\t[SCALES]: {}'.format(s_animal)
        s_scale_armor += armor_weirdness(400, f_gas_rand_base_price)


    return s_scale_armor

def generate_armor_wood_and_metal(s_mat,
                                     i_dieroll,
                                     s_gawam_rand_base_price):
    s_wood_and_metal = ''

    s_wood_and_metal += generate_armor_metal(s_mat,
                                             i_dieroll,
                                             s_gawam_rand_base_price)
    s_wood_and_metal += generate_armor_wooden(s_mat,
                                             i_dieroll,
                                             s_gawam_rand_base_price)


    return s_wood_and_metal

def generate_armor_fur(s_mat,
                         i_dieroll,
                         f_gaf_rand_base_price):

    s_fur_type = ''
    s_fur_armor = ''
    l_fur = config.csv_to_list('data/armor/armor_fur_type.csv')

    i_gaf_dieroll = random.randint(1, i_dieroll)

    for line in l_fur:
        if i_gaf_dieroll <= int(line['DieRoll']):
            s_fur_type += line['Fur Type']
            break

    if s_fur_type == 'Common':
        l_common_fur = []
        l_common = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Fur':
                l_common_fur.append(line)

        i_common_fur_roll = random.randint(0, len(l_common_fur)-1)
        s_animal = l_common_fur[i_common_fur_roll]['Animal']
        s_fur_armor += u'\n\t\t[FUR]: {}'.format(s_animal)

    elif s_fur_type == 'Rare':
        l_rare_fur = []
        l_rare = config.csv_to_list('data/goods/goods_Rare_animals.csv')

        for line in l_rare:
            if line['Hide'] == 'Fur':
                l_rare_fur.append(line)

        i_rare_fur_roll = random.randint(0, len(l_rare_fur)-1)
        s_animal = l_rare_fur[i_rare_fur_roll]['Animal']
        s_fur_armor += u'\n\t\t[FUR]: {}'.format(s_animal)

    elif s_fur_type == 'Magical':
        l_magical_fur = []
        l_magical = config.csv_to_list('data/goods/goods_magical_animals.csv')

        for line in l_magical:
            if line['Hide'] == 'Fur':
                l_magical_fur.append(line)

        i_magical_fur_roll = random.randint(0, len(l_magical_fur)-1)
        s_animal = l_magical_fur[i_magical_fur_roll]['Animal']
        s_fur_armor += u'\n\t\t[FUR]: {}'.format(s_animal)

    elif s_fur_type == 'Common Special':
        l_common_fur = []
        l_common = config.csv_to_list('data/goods/goods_common_animals.csv')

        for line in l_common:
            if line['Hide'] == 'Fur':
                l_common_fur.append(line)

        i_common_fur_roll = random.randint(0, len(l_common_fur)-1)
        s_animal = l_common_fur[i_common_fur_roll]['Animal']
        s_fur_armor += u'\n\t\t[FUR]: {}'.format(s_animal)
        s_fur_armor += armor_weirdness(400, f_gaf_rand_base_price)

    elif s_fur_type == 'Rare Special':
        l_rare_fur = []
        l_rare = config.csv_to_list('data/goods/goods_Rare_animals.csv')

        for line in l_rare:
            if line['Hide'] == 'Fur':
                l_rare_fur.append(line)

        i_rare_fur_roll = random.randint(0, len(l_rare_fur)-1)
        s_animal = l_rare_fur[i_rare_fur_roll]['Animal']
        s_fur_armor += u'\n\t\t[FUR]: {}'.format(s_animal)
        s_fur_armor += armor_weirdness(400, f_gaf_rand_base_price)

    elif s_fur_type == 'Magical Special':
        l_magical_fur = []
        l_magical = config.csv_to_list('data/goods/goods_magical_animals.csv')

        for line in l_magical:
            if line['Hide'] == 'Fur':
                l_magical_fur.append(line)

        i_magical_fur_roll = random.randint(0, len(l_magical_fur)-1)
        s_animal = l_magical_fur[i_magical_fur_roll]['Animal']
        s_fur_armor += u'\n\t\t[FUR]: {}'.format(s_animal)
        s_fur_armor += armor_weirdness(400, f_gaf_rand_base_price)

    return s_fur_armor