#!/usr/bin/env python
""" Functions for determining types of art.


"""

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules

import csv
import random

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"

# TODO: Randomize the value of the [COLLECTOR'S VALUE] entry.


# noinspection PyPep8,PyPep8,PyPep8,PyPep8,PyUnboundLocalVariable
def art_type(f_art_adj_treasure_unit):
    """Determine the type of art, call other art functions
    :type f_art_adj_treasure_unit: float
    :rtype: str
    """

    # Let's set up some variables!

    # A list (of dicts) for every file.
    l_age = []
    l_comp_quality = []
    l_condition = []
    l_quality = []
    l_renown = []
    l_size = []
    l_subject = []
    l_type = []
    l_type_carving = []
    l_type_ceramics = []
    l_type_craft = []
    l_type_fabric = []
    l_type_glasswork = []
    l_type_metalwork = []
    l_type_painting = []
    l_type_paper = []
    l_type_stonework = []
    l_type_value_categories = []
    l_type_overflow = []
    # A counter for every list.
    i_age_ctr = 0
    i_comp_quality_ctr = 0
    i_condition_ctr = 0
    i_quality_ctr = 0
    i_renown_ctr = 0
    i_size_ctr = 0
    i_subject_ctr = 0
    i_type_ctr = 0
    i_type_carving_ctr = -1
    i_type_ceramics_ctr = -1
    i_type_craft_ctr = -1
    i_type_fabric_ctr = -1
    i_type_glasswork_ctr = -1
    i_type_metalwork_ctr = -1
    i_type_painting_ctr = -1
    i_type_paper_ctr = -1
    i_type_stonework_ctr = -1
    i_type_value_categories_ctr = -1
    # and finally, the return value.
    s_art_object = ''

    # Note: this next part is going to suck.  csv.DictReader objects are NOT
    # indexable.  my_dict_reader['column_A]['column_B'] raises an exception.
    # so, what we are going to do is turn them into lists and let DictReader
    # build the component dictionaries for us.

    # Also note: This is stupid.
    with open('data/art/art_age.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_age_ctr +=1
            l_age.append(line)

    with open('data/art/art_composition_quality.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_comp_quality_ctr +=1
            l_comp_quality.append(line)

    with open('data/art/art_condition.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_condition_ctr +=1
            l_condition.append(line)

    with open('data/art/art_quality.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_quality_ctr +=1
            l_quality.append(line)

    with open('data/art/art_renown.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_renown_ctr +=1
            l_renown.append(line)

    with open('data/art/art_size.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_size_ctr +=1
            l_size.append(line)

    with open('data/art/art_subject.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_subject_ctr +=1
            l_subject.append(line)

    with open('data/art/art_type.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_type_ctr +=1
            l_type.append(line)

    with open('data/art/art_value_categories.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            i_type_value_categories_ctr +=1
            l_type_value_categories.append(line)

    with open('data/art/art_type_listings.csv', newline='') as art_f:
        d_art = csv.DictReader(art_f, delimiter=',', skipinitialspace=True)
        for line in d_art:
            if line['Type'] == 'Metalwork':
                i_type_metalwork_ctr +=1
                l_type_metalwork.append(line)
            elif line['Type'] == 'Painting':
                i_type_painting_ctr +=1
                l_type_painting.append(line)
            elif line['Type'] == 'Paper':
                i_type_paper_ctr +=1
                l_type_paper.append(line)
            elif line['Type'] == 'Stonework':
                i_type_stonework_ctr +=1
                l_type_stonework.append(line)
            elif line['Type'] == 'Fabric':
                i_type_fabric_ctr +=1
                l_type_fabric.append(line)
            elif line['Type'] == 'Craft':
                i_type_craft_ctr +=1
                l_type_craft.append(line)
            elif line['Type'] == 'Glasswork':
                i_type_glasswork_ctr +=1
                l_type_glasswork.append(line)
            elif line['Type'] == 'Ceramics':
                i_type_ceramics_ctr +=1
                l_type_ceramics.append(line)
            elif line['Type'] == 'Carving':
                i_type_carving_ctr +=1
                l_type_carving.append(line)
            else:
                l_type_overflow.append(line)

    # Find the initial value category of the treasure.
    # We're going to add the modifiers to it to get a new category.
    for line in l_type_value_categories:
        i_value_category = 0
        if f_art_adj_treasure_unit <= float(line['Value']):
            i_value_category = int(line['Number'])
            break

    # Get the quality ratings and modifiers from the lists.
    i_age_roll = random.randint(1, 400)
    i_comp_quality_roll = random.randint(1, 400)
    i_condition_roll = random.randint(1, 400)
    i_quality_roll = random.randint(1, 400)
    i_renown_roll = random.randint(1, 400)
    i_size_roll = random.randint(1, 400)
    i_subject_roll = random.randint(1, 400)
    i_type_roll = random.randint(1, 400)
    i_type_carving_roll = random.randint(0, i_type_carving_ctr)
    i_type_ceramics_roll = random.randint(0, i_type_ceramics_ctr)
    i_type_craft_roll = random.randint(0, i_type_craft_ctr)
    i_type_fabric_roll = random.randint(0, i_type_fabric_ctr)
    i_type_glasswork_roll = random.randint(0, i_type_glasswork_ctr)
    i_type_metalwork_roll = random.randint(0, i_type_metalwork_ctr)
    i_type_painting_roll = random.randint(0, i_type_painting_ctr)
    i_type_paper_roll = random.randint(0, i_type_paper_ctr)
    i_type_stonework_roll = random.randint(0, i_type_stonework_ctr)

    # What TYPE of art is it?
    i_l_type_ctr = -1
    for line in l_type:
        i_l_type_ctr += 1
        if i_type_roll <= int(line['DieRoll']):
            s_art_object = '\n\t[ART MEDIUM]: {}.'.format(line['Art Type'])
            break

    # OK, now, what kind of artwork of that type is it?
    # TODO: Add subtables - Paper Arts, Ceramics, Glasswork, &ct.
    if l_type[i_l_type_ctr]['Art Type'] == 'Carving':
        for line in l_type_carving:
            if i_type_carving_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Fabric':
        for line in l_type_fabric:
            if i_type_fabric_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Painting':
        for line in l_type_painting:
            if i_type_painting_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Crafts':
        for line in l_type_craft:
            if i_type_craft_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Paper':
        for line in l_type_paper:
            if i_type_paper_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Ceramics':
        for line in l_type_ceramics:
            if i_type_ceramics_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Glasswork':
        for line in l_type_glasswork:
            if i_type_glasswork_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Stonework':
        for line in l_type_stonework:
            if i_type_stonework_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    elif l_type[i_l_type_ctr]['Art Type'] == 'Metalwork':
        for line in l_type_metalwork:
            if i_type_metalwork_roll <= int(line['DieRoll']):
                s_style = '\n\t\t[STYLE]: {}.'.format(line['Style'])
                break

    else:
        s_style = '[TYPE] WeIRd aND CuThULUoiD. [STYLE]: Mind breaking.'
    # Now assign it!
    s_art_object += s_style

    # Get the modifiers for the artwork's value, then add 'em up.

    for line in l_age:
        if i_age_roll <= int(line['DieRoll']):
            s_age = '\n\t\t\t[Age]: {}.'.format(line['Age'])
            i_age_mod = int(line['Modifier'])
            break

    for line in l_comp_quality:
        if i_comp_quality_roll <= int(line['DieRoll']):
            s_comp_qlty = '\n\t\t\t[Composition Quality]: {}.'.format(line[
                'Composition'])
            i_comp_qlty_mod = int(line['Modifier'])
            break

    for line in l_condition:
        if i_condition_roll <= int(line['DieRoll']):
            s_condition = '\n\t\t\t[Condition]: {}.'.format(line['Condition'])
            i_condition_mod = int(line['Modifier'])
            break

    for line in l_quality:
        if i_quality_roll <= int(line['DieRoll']):
            s_quality = '\n\t\t\t[Materials Quality]: {}.'.format(line[
                'Quality'])
            i_quality_mod = int(line['Modifier'])
            break

    for line in l_renown:
        if i_renown_roll <= int(line['DieRoll']):
            s_renown = "\n\t\t\t[Artists' Renown]: {}.".format(line['Renown'])
            i_renown_mod = int(line['Modifier'])
            break

    for line in l_size:
        if i_size_roll <= int(line['DieRoll']):
            s_size = '\n\t\t\t[Size]: {}.'.format(line['Size'])
            i_size_mod = int(line['Modifier'])
            break

    for line in l_subject:
        if i_subject_roll <= int(line['DieRoll']):
            s_subject = '\n\t\t[SUBJECT]: {}.'.format(line['Subject'])
            i_subject_mod = int(line['Modifier'])
            break

    # Find out how much it's worth...
    i_full_modifier = (i_age_mod + i_comp_qlty_mod + i_condition_mod +
            i_quality_mod + i_renown_mod + i_size_mod + i_subject_mod +
            i_value_category)

    if i_full_modifier > i_type_value_categories_ctr:
        i_full_modifier = i_type_value_categories_ctr
    if i_full_modifier < 0:
        i_full_modifier = 0

    s_art_value = "\n\t\t[COLLECTOR'S VALUE]: {:,.2f}sp.".format(
        float(l_type_value_categories[i_full_modifier]['Value']))

    s_art_object += '{}{}{}{}{}{}{}{}\n'.format(s_subject, s_art_value,
        s_comp_qlty, s_renown, s_size,
        s_age, s_condition, s_quality)
    return s_art_object

