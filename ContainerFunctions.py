#!/usr/bin/env python
""" File for container methods.
NOTE: Trap and Hidden rolls are ARTIFICIALLY HIGH for testing purposes.
Live code should reduce chances to 1 or 2 in 20.
"""

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules

import csv
import random
# import RoomStockingFunctions as RoomStockFunc
from config import max_dict

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"






def container_type():
    """Determine the type of container: bag, trunk &ct.
    :rtype : str
    """

    with open('data/treasuretypes/treasure_containers.csv', newline='') as \
            tc_f:

        d_tc = csv.DictReader(tc_f, delimiter=',', skipinitialspace=True)

        y_ct = random.randint(1, 20)
        # TODO: pull limit of randint using max_dict()

        for line in d_tc:
            dieroll = int(line['DieRoll'])
            s_container = line['Container']

            if y_ct <= dieroll:

                if s_container == 'Bags or sacks':
                    s_bags = container_bags()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container,
                                                           s_bags)


                elif s_container == 'Barrels or Cask':
                    s_chest = container_chest()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container,
                                                           s_chest)


                elif s_container == 'Coffer or Kist':
                    s_chest = container_chest()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container,
                                                           s_chest)


                elif s_container == 'Chest':
                    s_chest = container_chest()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container,
                                                           s_chest)


                elif s_container == 'Huge Chest':
                    s_chest = container_chest()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container,
                                                           s_chest)


                elif s_container == 'Trunk':
                    s_chest = container_chest()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container,
                                                           s_chest)

                elif s_container == 'Urn':
                    s_jar = container_jar()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container, s_jar)

                elif s_container == 'Jar':
                    s_jar = container_jar()
                    i = u'\n[CONTAINER]: {0}. {1}.'.format(s_container, s_jar)

                elif s_container == 'Niche':
                    s_niche = container_niche()
                    i = u'\n[STRUCTURE]: {0}. {1}.'.format(s_container,
                                                          s_niche)

                elif s_container == 'Loose':
                    s_loose = container_loose()
                    i = u'\n[ORGANIZATION]: {0}. {1}.'.format(s_container,
                                                          s_loose)
                else:
                    i = u'\n[CONTAINER]: {0}.'.format(s_container)


                return i


def container_hidden():
    """Return '[HIDDEN]: something 1/20 chance else return 'Not Hidden'.
    :rtype : str
    """

    with open('data/treasuretypes/treasure_hidden.csv', newline='') as th_f:

        d_th = csv.DictReader(th_f, delimiter=',', skipinitialspace=True)
        next(d_th)

        j_th = max_dict(d_th)

        y_th = random.randint(1, j_th)

        th_f.seek(0)
        d_th = csv.DictReader(th_f, delimiter=',', skipinitialspace=True)

        for line in d_th:
            dieroll = int(line['DieRoll'])
            s_hidden_by = str(line['HiddenBy'])

            if y_th <= dieroll:
                i = u'\n[HIDDEN]: {0}'.format(s_hidden_by)
                return i
            else:
                i = "[HIDDEN]: ERROR, we should never get here!"

                # return str: i


def container_trapped():
    """Return '[Trapped]: something 1/20 chance else return 'Not Trapped'.
    :rtype : str
    """

    with open('data/treasuretypes/treasure_trapped.csv', newline='') as \
            ttrap_f:
        d_ttrap = csv.DictReader(ttrap_f, delimiter=',', skipinitialspace=True)
        next(d_ttrap)

        j_ttrap = max_dict(d_ttrap)

        y_ttrap = random.randint(1, j_ttrap)

        ttrap_f.seek(0)
        d_ttrap = csv.DictReader(ttrap_f, delimiter=',', skipinitialspace=True)

        for a_line in d_ttrap:
            dieroll = int(a_line['DieRoll'])
            s_trapped_with = str(a_line['TrappedWith'])

            if y_ttrap <= dieroll:
                i = '\n[TRAPPED]: {0}.'.format(str(s_trapped_with))
                return i
            else:
                i = '[TRAPPED]: ERROR, we should never get here.'

                # return str i


def container_bags():
    """Determine the type of bag using the
    treasure_containers_bags.csv file.
    :rtype : str
    """
    with open('data/treasuretypes/treasure_containers_bags.csv', newline='') \
            as tcb_f:
        d_tcb = csv.DictReader(tcb_f, delimiter=',', skipinitialspace=True)

        l_bags = []
        l_seals = []
        i_seals_ctr = 0
        i_bags_ctr = 0
        s_bag_type = ''

        # TODO: Bag and seal type by item availability.

        # First, split d_tcb into l_seals and l_bags
        for line in d_tcb:
            # Renumber DieRoll from 1 - N (however many seals there are)
            if line['Type'] == 'Seal':
                i_seals_ctr += 1
                line['DieRoll'] = str(i_seals_ctr)
                l_seals.append(line)

            else:
                i_bags_ctr += 1
                l_bags.append(line)

        i_bags_roll = random.randint(0, i_bags_ctr-1)
        i_seals_roll = random.randint(0, i_seals_ctr-1)

        s_bag_type = u'\n\t[CLOTH TYPE]: {0}'.format(str(l_bags[
            i_bags_roll]['Type']))
        s_bag_type += u'\n\t[FABRIC]: {0}'.format(str(l_bags[
                    i_bags_roll]['Fabric']))
        s_bag_type += u'.\n\t[SEAL]: {0}'.format(str(l_seals[i_seals_roll][
            'Fabric']))

        return s_bag_type


def container_chest():
    """Determine the material of the chest using the
    treasure_containers_chests.csv file.
    :rtype : str
    """
    with open('data/treasuretypes/treasure_containers_chests.csv',
              newline='') as tcc_f:
        d_tcc = csv.DictReader(tcc_f, delimiter=',', skipinitialspace=True)

        # tcc_f_name = tcc_f.name
        l_chests = []
        l_seals = []
        i_seals_ctr = 0
        i_chests_ctr = 0
        s_chests_type = ''

        # TODO: Chest and seal type by item availability.

        # First, split d_tcb into l_seals and l_chests
        for line in d_tcc:
            # Renumber DieRoll from 1 - N (however many seals there are)
            if line['Type'] == 'Seal':
                i_seals_ctr += 1
                line['DieRoll'] = str(i_seals_ctr)
                l_seals.append(line)

            else:
                i_chests_ctr += 1
                l_chests.append(line)

        i_chests_roll = random.randint(1, i_chests_ctr-1)
        i_seals_roll = random.randint(1, i_seals_ctr-1)

        s_chests_type += u'\n\t[MATERIAL]: {0}'.format(str(l_chests[
                    i_chests_roll]['Material']))
        s_chests_type += u'.\n\t[SEAL]: {0}'.format(str(l_seals[
            i_seals_roll]['Material']))

        return s_chests_type


def container_jar():
    """Determine the material of the jar using the
    treasure_containers_jars.csv file.
    :rtype : str
    """
    with open('data/treasuretypes/treasure_containers_jars.csv', newline='') \
            as tcj_f:
        d_tcj = csv.DictReader(tcj_f, delimiter=',', skipinitialspace=True)

        l_jars = []
        l_seals = []
        i_seals_ctr = 0
        i_jars_ctr = 0
        s_jars_type = ''

        # TODO: Jar and seal type by item availability.

        # First, split d_tcb into l_seals and l_jars
        for line in d_tcj:
            # Renumber DieRoll from 1 - N (however many seals there are)
            if line['Type'] == 'Seal':
                i_seals_ctr += 1
                line['DieRoll'] = str(i_seals_ctr)
                l_seals.append(line)

            else:
                i_jars_ctr += 1
                l_jars.append(line)

        i_jars_roll = random.randint(0, i_jars_ctr-1)
        i_seals_roll = random.randint(0, i_seals_ctr-1)

        s_jars_type += u'\n\t[MATERIAL]: {0}'.format(str(l_jars[
                    i_jars_roll]['Material']))
        s_jars_type += u'.\n\t[SEAL]: {0}'.format(str(l_seals[
            i_seals_roll]['Material']))

        return s_jars_type


def container_niche():
    """Determine the location of the niche using the
    treasure_containers_niches.csv.csv file.
    :rtype : str
    """
    with open('data/treasuretypes/treasure_containers_niches.csv',
              newline='') as \
            tcn_f:
        d_tcn = csv.DictReader(tcn_f, delimiter=',', skipinitialspace=True)

        l_niches = []
        l_seals = []
        i_seals_ctr = 0
        i_niches_ctr = 0
        s_niches_type = ''

        # TODO: Niche and seal type by item availability.

        # First, split d_tcb into l_seals and l_niches
        for line in d_tcn:
            # Renumber DieRoll from 1 - N (however many seals there are)
            if line['Type'] == 'Seal':
                i_seals_ctr += 1
                line['DieRoll'] = str(i_seals_ctr)
                l_seals.append(line)

            else:
                i_niches_ctr += 1
                l_niches.append(line)

        i_niches_roll = random.randint(0, i_niches_ctr-1)
        i_seals_roll = random.randint(0, i_seals_ctr-1)
        i_lit_roll = random.randint(1, 5)

        if i_lit_roll <= 2:
            s_niches_type += u'\n\t[LIT]: Yes.'
        else:
            s_niches_type += u'\n\t[LIT]: No.'

        s_niches_type += u'\n\t[LOCATION]: {0}'.format(str(l_niches[
                    i_niches_roll]['Location']))
        s_niches_type += u'.\n\t[SEAL]: {0}'.format(str(l_seals[
            i_seals_roll]['Location']))

        return s_niches_type


def container_loose():
    """Determine the stacking of the loose treasure using the
    treasure_containers_loose.csv.csv file.
    NOTE: Seal type code "if'ed" out, but not deleted in case
    we come up with seal types.
    :rtype : str
    """
    with open('data/treasuretypes/treasure_containers_loose.csv',
              newline='') as tcl_f:
        d_tcl = csv.DictReader(tcl_f, delimiter=',', skipinitialspace=True)

        l_loose = []
        l_seals = []
        i_seals_ctr = 0
        i_loose_ctr = 0
        s_loose_type = ''

        # TODO: Seal type (only) by item availability.

        # First, split d_tcb into l_seals and l_bags
        for line in d_tcl:
            # Renumber DieRoll from 1 -> N (however many seals there are)
            if line['Type'] == 'Seal':
                i_seals_ctr += 1
                line['DieRoll'] = str(i_seals_ctr)
                l_seals.append(line)

            else:
                i_loose_ctr += 1
                l_loose.append(line)

        # Watch for index out-of bounds, *_ctr = *_ctr - 1
        i_loose_roll = random.randint(0, i_loose_ctr-1)
        i_seals_roll = random.randint(0, i_seals_ctr-1)

        s_loose_type += u'\n\t[LOCATION]: {0}'.format(str(l_loose[
            i_loose_roll]['Location']))

        if len(l_seals) != 0:
        # No seals at the moment, but if there are this code will then run.
            s_loose_type += u'.\n\t[SEAL]: {0}'.format(str(l_seals[
                i_seals_roll-1]['Location']))

        return s_loose_type