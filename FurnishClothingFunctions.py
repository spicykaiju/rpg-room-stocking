#! /usr/bin/env python
""" Functions for placing Furnishings and Clothing entries.


"""

# import statements
import random
import GemFunctions as GemFunc
from config import csv_to_list
import textwrap

__author__ = 'Roger E. Burgess, III'
__copyright__ = "Copyright 2014, Red Flag & Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or later"
__version__ = "0.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"


def furnish_cloth_main(f_split_value):
    """ Does all the work of assinging strings to pass to
    RoomStockingFunctions.py.

    :param f_split_value: float
    :rtype s_furnishings: str
    """

    l_clothing = []
    l_cloth_enhancements = []
    l_furniture = []
    l_rugs_csv = []
    l_rugs_style = []
    l_rugs_weave = []
    l_stonework = []
    l_furniture_enhance = []
    s_furnishings = ''
    i_enhancement_type = 0
    cloth_or_furniture = random.randint(1, 6)

    # Clothing
    if cloth_or_furniture > 4:
        l_clothing = csv_to_list('data/cloth&furn/clothing_type.csv')
        i_clothing_roll = random.randint(1, 100)

        l_fabric_type = csv_to_list('data/goods/goods_fabrics.csv')
        i_fabric_roll = random.randint(0, len(l_fabric_type) - 1)

        for line in l_clothing:
            if i_clothing_roll <= int(line['DieRoll']):
                s_clothing_type = line['Type']
                break

        s_furnishings += '\n\t[CLOTHING]: {}\n'.format(s_clothing_type)
        s_furnishings += '\t\t[WEAVE]: {}\n'.format(
            l_fabric_type[i_fabric_roll]['Type'])
        s_furnishings += '\t\t[FABRIC]: {}\n'.format(
            l_fabric_type[i_fabric_roll]['Fabric'])


    # Furniture

    else:
        l_furniture = csv_to_list('data/cloth&furn/furnishings.csv')

        l_furniture_enhance = csv_to_list(
            'data/cloth&furn/furnishing_enhancements.csv')

        # Select the type of furniture:
        i_furniture_type = random.randint(1, 100)
        s_furn_type = ''
        for line in l_furniture:
            if i_furniture_type <= int(line['DieRoll']):
                s_furn_type = line['Type']
                s_furn_desc = u'\n\t[FURNISHING]: {}'.format(line['Type'])
                s_furn_mat  = line['Material']
                s_furnishings += s_furn_desc
                break

        # Deal with carpets
        # TODO: (Maybe) Add colors to carpets.
        if s_furn_mat == 'Rug':
            l_rugs_csv = csv_to_list('data/cloth&furn/furnishing_rugs.csv')
            # Split styles and weaves
            for line in l_rugs_csv:
                if line['Type'] == 'Style':
                    l_rugs_style.append(line)

            else:
                l_rugs_weave.append(line)

            i_rug_style_roll = random.randint(0, len(l_rugs_style) - 1)
            i_rug_weave_roll = random.randint(0, len(l_rugs_weave) - 1)

            s_rug_style = u'\n\t\t[RUG]: Made in the {}.'.format(
                l_rugs_style[i_rug_style_roll]['Name'])
            s_rug_weave = u'\n\t\t[WEAVE]: {}.\n'.format(
                l_rugs_weave[i_rug_weave_roll]['Name'])

            s_furnishings += s_rug_style + s_rug_weave


        # Deal with things made of stone
        elif s_furn_mat == 'Stone':
            i_enhancement_type = random.randint(0, len(l_furniture_enhance)-1)
            l_stonework = csv_to_list('data/goods/goods_stone.csv')
            i_stone_roll = random.randint(0, len(l_stonework) - 1)

            # stone_name = (u'\n\t\t[STOME]: ' + l_stone[i_random_stone]['Name'])
            # stone_desc = (u'' + l_stone[i_random_stone]['Description'])
            # stone_type = (l_stone[i_random_stone]['Type'])
            #
            # st_desc_wrap = textwrap.fill(stone_desc,
            #                      initial_indent='        ',
            #                      subsequent_indent='        ')
            #
            # sg_goods += (u'{}. {}\n{}'.format(stone_name, stone_type, st_desc_wrap))


            s_furnishings += '\n\t\t[STONE]: {}. {}.'.format(
                l_stonework[i_stone_roll]['Name'],
                l_stonework[i_stone_roll]['Type'])
            stone_desc = (u'' + l_stonework[i_stone_roll]['Description'])
            st_desc_wrap = textwrap.fill(stone_desc,
                                  initial_indent='        ',
                                  subsequent_indent='        ')
            s_furnishings += (u'\n' + st_desc_wrap)

            s_furnishings += '\n\t\t[ENHANCEMENT]: {}\n'.format(
                l_furniture_enhance[i_enhancement_type]['Enhancement'])

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Inlaid':
                l_metal_type = csv_to_list('data/goods/goods_metals.csv')
                i_metals_roll = random.randint(0, len(l_metal_type) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_metal_type[i_metals_roll]['Metal'])
                s_metal_desc = l_metal_type[i_metals_roll]['Description']
                s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_metal_wrap)

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Wood or Ivory':
                l_rare_wood = csv_to_list('data/goods/goods_woods.csv')
                i_rare_wood_roll = random.randint(0, len(l_rare_wood) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_rare_wood[i_rare_wood_roll]['Wood Type'])
                s_wood_desc = l_rare_wood[i_rare_wood_roll]['Description']
                s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_wood_wrap)

        # Deal with things made of crystal
        elif s_furn_mat == 'Crystal':
            # load gems
            s_furnishings += (u'\n\t\t[CHANDELIER]: Contains the '
                              u'following gems.')
            s_furnishings += GemFunc.gem_main(f_split_value)

        # Deal with things made of wood
        elif s_furn_mat == 'Wood':
            i_enhancement_type = random.randint(0, len(l_furniture_enhance)-1)
            l_rare_wood = csv_to_list('data/goods/goods_woods.csv')
            i_rare_wood_roll = random.randint(0, len(l_rare_wood) - 1)

            s_furnishings += '\n\t\t[MATERIAL]: {}.'.format(
                l_rare_wood[i_rare_wood_roll]['Wood Type'])
            s_furnishings += '\n\t\t[NAME]: {}.'.format(
                l_rare_wood[i_rare_wood_roll]['Name'])
            s_wood_desc = u'[DESCRIPTION]: ' + l_rare_wood[i_rare_wood_roll]['Description']
            s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
            s_furnishings += u'\n\t\t' + s_wood_wrap
            s_furnishings += '\n\t\t[ENHANCEMENT]: {}\n'.format(
                l_furniture_enhance[i_enhancement_type]['Enhancement'])

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Inlaid':
                l_metal_type = csv_to_list('data/goods/goods_metals.csv')
                i_metals_roll = random.randint(0, len(l_metal_type) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_metal_type[i_metals_roll]['Metal'])
                s_metal_desc = l_metal_type[i_metals_roll]['Description']
                s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_metal_wrap)

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Wood':
                i_rare_wood_roll = random.randint(0, len(l_rare_wood) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_rare_wood[i_rare_wood_roll]['Name'])
                s_wood_desc = l_rare_wood[i_rare_wood_roll]['Description']
                s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_wood_wrap)

        # Deal with things made of metal
        elif s_furn_mat == 'Metal':
            i_enhancement_type = random.randint(0, len(l_furniture_enhance)-1)
            l_metal_type = csv_to_list('data/goods/goods_metals.csv')
            i_metals_roll = random.randint(0, len(l_metal_type) - 1)

            s_furnishings += '\n\t\t[METALWORK]: {}'.format(
                l_metal_type[i_metals_roll]['Metal'])
            s_furnishings += '\n\t\t[ENHANCEMENT]: {}\n'.format(
                l_furniture_enhance[i_enhancement_type]['Enhancement'])

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Inlaid':
                i_metals_roll = random.randint(0, len(l_metal_type) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_metal_type[i_metals_roll]['Metal'])
                s_metal_desc = l_metal_type[i_metals_roll]['Description']
                s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_metal_wrap)

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Wood':
                l_rare_wood = csv_to_list('data/goods/goods_woods.csv')
                i_rare_wood_roll = random.randint(0, len(l_rare_wood) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_rare_wood[i_rare_wood_roll]['Name'])
                s_wood_desc = l_rare_wood[i_rare_wood_roll]['Description']
                s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_wood_wrap)


        # Deal with things made of fabric
        elif s_furn_mat == 'Fabric':
            l_fabric_type = csv_to_list('data/goods/goods_fabrics.csv')
            i_fabric_roll = random.randint(0, len(l_fabric_type) - 1)

            s_furnishings += '\n\t\t[WEAVE]: {}'.format(
                l_fabric_type[i_fabric_roll]['Type'])
            s_furnishings += '\n\t\t[FABRIC]: {}\n'.format(
                l_fabric_type[i_fabric_roll]['Fabric'])

        # Deal with the rest
        else:
            i_enhancement_type = random.randint(0, len(l_furniture_enhance)-1)


            s_furnishings += '\n\t\t[ENHANCEMENT]: {}\n'.format(
                l_furniture_enhance[i_enhancement_type]['Enhancement'])

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Inlaid':
                l_metal_type = csv_to_list('data/goods/goods_metals.csv')
                i_metals_roll = random.randint(0, len(l_metal_type) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_metal_type[i_metals_roll]['Metal'])
                s_metal_desc = l_metal_type[i_metals_roll]['Description']
                s_metal_wrap = textwrap.fill(s_metal_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_metal_wrap)

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Wood':
                l_rare_wood = csv_to_list('data/goods/goods_woods.csv')
                i_rare_wood_roll = random.randint(0, len(l_rare_wood) - 1)

                s_furnishings += '\t\t[INLAID WITH]: {}\n'.format(
                    l_rare_wood[i_rare_wood_roll]['Name'])
                s_wood_desc = l_rare_wood[i_rare_wood_roll]['Description']
                s_wood_wrap = textwrap.fill(s_wood_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
                s_furnishings += u'\n\t\t[DESCRIPTION]: {}'.format(s_wood_wrap)

            if l_furniture_enhance[i_enhancement_type]['Enhancement'] == 'Upholstered':
                l_fabric_type = csv_to_list('data/goods/goods_fabrics.csv')
                i_fabric_roll = random.randint(0, len(l_fabric_type) - 1)

                s_furnishings += '\t\t[WEAVE]: {}'.format(
                    l_fabric_type[i_fabric_roll]['Type'])
                s_furnishings += '\t\t[FABRIC]: {}\n'.format(
                    l_fabric_type[i_fabric_roll]['Fabric'])


    return s_furnishings



