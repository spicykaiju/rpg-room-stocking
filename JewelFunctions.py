#!/usr/bin/env python
"""Functions for creating jewels and gems.


"""

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"

import csv
import random
# import RoomStockingFunctions as RoomStockFunc

# TODO: rationalize the values of the [CONSTRUCTION] entries
# TODO: rationalize the values of the [OTHER] entries
# TODO: Magical results are far, FAR too common.
# TODO: Create an enhancement table. fill jeweled with gem_main()

def populate_utensils():
    with open('data/jeweled_items.csv', newline='') as ji_f:
        d_ji = csv.DictReader(ji_f, delimiter=',', skipinitialspace=True)

        lf_utensil = []
        if_utensil_ctr = 0

        for line in d_ji:
            if line['Type'] == 'Utensil':
                if_utensil_ctr += 1
                line['DieRoll'] = str(if_utensil_ctr)
                lf_utensil.append(line)

        return lf_utensil, if_utensil_ctr

def populate_jewelery():
    with open('data/jeweled_items.csv', newline='') as ji_f:
        d_ji = csv.DictReader(ji_f, delimiter=',', skipinitialspace=True)

        lf_jeweled_object = []
        if_jeweled_ctr = 0

        for line in d_ji:
            if line['Type'] == 'Jewelry':
                if_jeweled_ctr += 1
                line['DieRoll'] = str(if_jeweled_ctr)
                lf_jeweled_object.append(line)

        return lf_jeweled_object, if_jeweled_ctr

def populate_quality():
    with open('data/jeweled_items.csv', newline='') as ji_f:
        d_ji = csv.DictReader(ji_f, delimiter=',', skipinitialspace=True)

        lf_quality = []
        if_quality_ctr = 0

        for line in d_ji:
            if line['Type'] == 'Quality':
                if_quality_ctr += 1
                line['DieRoll'] = str(if_quality_ctr)
                lf_quality.append(line)

        return lf_quality, if_quality_ctr

def populate_overflow():
    with open('data/jeweled_items.csv', newline='') as ji_f:
        d_ji = csv.DictReader(ji_f, delimiter=',', skipinitialspace=True)

        lf_overflow = []
        if_overflow_ctr = 0

        for line in d_ji:
            if line['Type'] != ('Jewelry' or 'Quality' or 'Utensil'):
                if_overflow_ctr += 1
                line['DieRoll'] = str(if_overflow_ctr)
                lf_overflow.append(line)

        return lf_overflow, if_overflow_ctr

def jewelry_type():
    """Determine the type of jewelery.
    :rtype: str
    """

    l_overflow, i_overflow_ctr = populate_overflow()
    l_quality, i_quality_ctr = populate_quality()
    l_jeweled_object, i_jeweled_ctr = populate_jewelery()
    l_utensil, i_utensil_ctr = populate_utensils()
    s_jewelery_type = ''
    i_jeweled_roll = random.randint(1, int(l_jeweled_object[
        i_jeweled_ctr-1]['DieRoll']))
    i_utensil_roll = random.randint(0, i_utensil_ctr-1)
    i_quality_roll = random.randint(0, i_quality_ctr-1)

    for line in l_jeweled_object:
        dieroll = int(line['DieRoll'])

        if i_jeweled_roll <= dieroll:
            s_jewelery_type = ''
            s_jewelery_type += u'\n\t[JEWELRY]: {}.'.format(str(line[
                'Object']))

            if str(line['Object']) == 'Utensils':
                s_jewelery_type += u'\n\t\t[UTENSIL TYPE]: {}.'.format(
                    str(l_utensil[i_utensil_roll]['Object']))

            break

    if i_overflow_ctr > 0:
        i_overflow_roll = random.randint(1, i_overflow_ctr-1)

    s_jewelery_type += u'\n\t\t[CONSTRUCTION]: {}.'.format(str(l_quality[
        i_quality_roll]['Object']))

    if len(l_overflow) > 0:
        s_jewelery_type += u'\n\t\t[Notable motif]: {}.\n'.format(str(l_overflow[
            i_overflow_roll]['Object']))

    return s_jewelery_type
