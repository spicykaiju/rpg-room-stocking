#!/usr/bin/env python
""" Just some global variables and minor utility strings.


"""


# TODO: pull the values of user_eras from \data\item_eras.
# TODO: replace hard coded random.randint(1, some_number) w/ max_dict()

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"

import csv
import random
dungeon_level = 1
user_era = 0
s_dl_dash = '\n===================\n'
s_sl_dash = '===================\n'
s_sl_dash_front = '\n==================='
s_dl_brace_dash = '\n[===================]\n'



def transposeDict(listOfDicts):
    """Turn a list of dicts into a dict of lists.  Assumes all dicts in the list have the exact same keys."""

    # keys = listOfDicts.iterkeys()
    return {[d['Key'] for d in listOfDicts]}


def item_eras(era_availability):
    """ Return a DictReader object with the contents of item_eras.csv
    with the headers "Key" and "Era".
    :rtype : list
    """
    with open('data/item_eras.csv', newline='') as ie_f:
        DR_ie = csv.DictReader(ie_f, delimiter=',', skipinitialspace=True)
        # l_eras = transposeDict(DR_ie)
        # return l_eras
        return DR_ie


def max_dict(dict_x):
    """ Return the highest value of column 'DieRoll' in 'some_thing.csv.
    :rtype : int
    """
    i_y = 0
    #next(dr)

    for line in dict_x:
        if 'DieRoll' in line:
            i_y = int(line['DieRoll'])
        if 'Number' in line:
            i_y = int(line['Number'])
    return i_y


def csv_to_list(f_location):
    """

    :rtype : list
    """
    l_return = []
    with open(f_location, newline='') as f_read_in:
        d_file = csv.DictReader(f_read_in, restval='None', delimiter=',',
                                skipinitialspace=True)
        for line in d_file:
            l_return.append(line)

    return l_return


def word_choice():
    """

    """
    s_file = 'data/dungeonwords/'
    l_choice = []
    l_choice = csv_to_list(s_file + 'wordchoice.csv')
    i_choice_rand = random.randint(1, 400)
    l_wordlist = []
    s_word = ''
    s_word_chosen = ''

    for line in l_choice:
        if i_choice_rand <= int(line['DieRoll']):
            s_word = line['Choice']
            break

    #Dungeon Wilder and West
    if s_word == 'Dungeon':
        l_wordlist = csv_to_list(s_file + 'dungeonwords.csv')
        s_word_chosen += '[DungeonWord]: '

    if s_word == 'Wilder':
        l_wordlist = csv_to_list(s_file + 'wilderwords.csv')
        s_word_chosen += '[WilderWord]: '

    if s_word == 'West':
        l_wordlist = csv_to_list(s_file + 'westwords.csv')
        s_word_chosen += '[WestWord]: '

    i_rand_word = random.randint(0, len(l_wordlist)-1)
    s_word_chosen += l_wordlist[i_rand_word]['Words']

    return s_word_chosen

def multiple_dungeon_word_choice():
    i_random_roll = random.randint(1, 100)
    s_word = ''

    if i_random_roll >= 95:
        s_first_choice = word_choice()
        s_second_choice = word_choice()
        s_word += (u'\n' + s_first_choice)
        s_word += (u'\n' + s_second_choice)
    else:
        s_only_choice = word_choice()
        s_word += (u'\n' + s_only_choice)

    return s_word